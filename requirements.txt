aiobotocore==2.1.0
aiofiles==0.8.0
aiohttp==3.8.1
aioitertools==0.9.0
aiosignal==1.2.0
alembic==1.7.6
anyio==3.5.0
asgiref==3.5.0
async-timeout==4.0.2
asyncpg==0.25.0
attrs==21.4.0
autopep8==1.6.0
bcrypt==3.2.0
botocore==1.23.24
certifi==2021.10.8
cffi==1.15.0
charset-normalizer==2.0.11
click==8.0.3
Deprecated==1.2.13
dnspython==2.2.0
ecdsa==0.17.0
email-validator==1.1.3
Faker==12.2.0
fastapi==0.73.0
fastapi-jwt-auth==0.5.0
fastapi-profiler==1.0.0
FastAPI-SQLAlchemy==0.2.1
flake8==4.0.1
frozenlist==1.3.0
greenlet==1.1.2
h11==0.13.0
idna==3.3
itsdangerous==2.0.1
Jinja2==3.0.3
jmespath==0.10.0
Mako==1.1.6
MarkupSafe==2.0.1
mccabe==0.6.1
multidict==6.0.2
packaging==21.3
passlib==1.7.4
Pillow==9.0.1
psycopg2-binary==2.9.3
pyasn1==0.4.8
pycodestyle==2.8.0
pycparser==2.21
pydantic==1.9.0
pydantic-sqlalchemy==0.0.9
pyflakes==2.4.0
pyinstrument==4.1.1
PyJWT==1.7.1
pyotp==2.6.0
pyparsing==3.0.7
python-dateutil==2.8.2
python-dotenv==0.19.2
python-jose==3.3.0
python-multipart==0.0.5
pytz==2021.3
redis==4.1.2
requests==2.27.1
rq==1.10.1
rsa==4.8
s3transfer==0.5.0
six==1.16.0
sniffio==1.2.0
SQLAlchemy==1.4.31
sqlalchemy-mptt==0.2.5
starlette==0.17.1
toml==0.10.2
twilio==7.5.1
typer==0.4.0
typing_extensions==4.0.1
urllib3==1.26.8
uvicorn==0.17.1
wrapt==1.13.3
yapf==0.32.0
yarl==1.7.2
