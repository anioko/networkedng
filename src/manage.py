from fastapi import FastAPI,  Request
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from fastapi_jwt_auth.exceptions import AuthJWTException
from api.routes import router
from config.config import settings
import typer
from pathlib import Path
from db.base import init_models
from utils.dep import db, redis_conn, redis_q
from models.user import User, Role
import uvicorn
from fastapi_sqlalchemy import DBSessionMiddleware
from fastapi.staticfiles import StaticFiles
from rq import Worker,Connection
from fastapi.templating import Jinja2Templates
import time
app = FastAPI(
    title=settings.APP_NAME,
    openapi_url=f'{settings.API_V1_STR}/openapi.json'
)



app.mount("/static/", StaticFiles(directory="static"), name="static")


BASE_PATH = Path(__file__).resolve().parent
TEMPLATES = Jinja2Templates(directory=str(BASE_PATH / "templates"))
#Templates = Jinja2Templates(directory=r"/templates")

app.add_middleware(DBSessionMiddleware, db_url=settings.DATABASE_URL)

app.include_router(
    router, prefix=settings.API_V1_STR
)


@app.exception_handler(AuthJWTException)
def authjwt_exception_handler(request: Request, exc: AuthJWTException):
    return JSONResponse(
        status_code=exc.status_code,
        content={"detail": exc.message}
    )

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)



cli = typer.Typer()


@cli.command()
def test_temp():
    if TEMPLATES.get_template('/account/login.html'):
        print('template found')
    print('template not found')    
    

@cli.command()
def db_init_models():
    init_models()
    print("Done")


@cli.command()
def setup_dev():
    """Runs the set-up needed for local development."""
    setup_general()


@cli.command()
def setup_prod():
    """Runs the set-up needed for production."""
    setup_general()


def setup_general():
    """Runs the set-up needed for both local development and production.
       Also sets up first admin user."""
    with db():
        Role.insert_roles()
        admin_query = db.session.query(Role).filter_by(name='Administrator')
        if admin_query.first() is not None:
            if db.session.query(User).filter_by(email=settings.ADMIN_EMAIL).first() is None:
                user = User(
                    first_name='Aniekan',
                    last_name='Okono',
            mobile_phone=settings.ADMIN_MOBILE_PHONE,
            area_code=settings.ADMIN_AREA_CODE,
                password_hash=settings.ADMIN_PASSWORD,
                confirmed=True,
                email=settings.ADMIN_EMAIL)
                db.session.add(user)
                db.session.commit()
                print('Added administrator {}'.format(user.full_name))


@cli.command()
def runserver():
    uvicorn.run("manage:app", host="127.0.0.1", port=9000, reload=True, debug=True,log_level="info")




@cli.command()
def add_fake_data():
    """
    Adds fake data to the database.
    """
    with db():
        start = time.time()
        User.generate_fake(count=500)
        end = time.time() - start
        print(end)


@cli.command()
def run_worker():
    """Initializes a slim rq task queue."""
    with Connection(redis_conn):
        worker = Worker(redis_q)
        worker.work()

if __name__ == "__main__":
    cli()
