from pydantic import BaseModel
from models.profiles import *
from pydantic_sqlalchemy import sqlalchemy_to_pydantic

ProfilePydantic = sqlalchemy_to_pydantic(Profile)
ProfileEduPydantic = sqlalchemy_to_pydantic(ProfileEdu)
ProfileJobPydantic = sqlalchemy_to_pydantic(ProfileJob)
ProfileLangPydantic = sqlalchemy_to_pydantic(ProfileLang)
ProfileProjectPydantic = sqlalchemy_to_pydantic(ProfileProject)


class ProfileSchema(ProfilePydantic):
    class Config:
        orm_mode = True

class ProfileEduSchema(ProfileEduPydantic):
    class Config:
        orm_mode = True


class ProfileJobSchema(ProfileJobPydantic):
    class Config:
        orm_mode = True


class ProfileLangSchema(ProfileLangPydantic):
    class Config:
        orm_mode = True


class ProfileProjectSchema(ProfileProjectPydantic):
    class Config:
        orm_mode = True