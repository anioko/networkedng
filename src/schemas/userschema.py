from typing import List, Optional, Text
from pydantic import BaseModel, EmailStr, HttpUrl
from datetime import datetime
from models.user import *
from models.extra import EditableHTML
from pydantic_sqlalchemy import sqlalchemy_to_pydantic

RolePydantic = sqlalchemy_to_pydantic
UserPydantic = sqlalchemy_to_pydantic(User)
Editable = sqlalchemy_to_pydantic(EditableHTML)
ContactPydantic= sqlalchemy_to_pydantic(ContactMessage)

class EditableSchema(Editable):
    class Config:
        orm_mode = True

class ContactSchema(ContactPydantic):
    id: Optional[int] 
    email: EmailStr
    created_at : Optional[datetime]
    updated_at : Optional[datetime]
    user_id : Optional[int]




class UserSchema(BaseModel):
    id: int
    email: str
    full_name: str
    mobile_phone:int
    photo: HttpUrl
    summary:str = None
    role:str
    area_code:str
    confirmed: bool


    class Config:
        orm_mode=True


class MessageSchema(BaseModel):
    id : int
    body : str
    recipient_id: int
    created_at: str

    class Config:
        orm_mode=True



class Login(BaseModel):
    mobile_phone: int
    password: str

