from typing import List
from models.blog import *
from .response import Pagination
from pydantic_sqlalchemy import sqlalchemy_to_pydantic

BlogSchema = sqlalchemy_to_pydantic(BlogPost)
BlogNewsLetterSchema = sqlalchemy_to_pydantic(BlogNewsLetter)
BlogCategorySchema = sqlalchemy_to_pydantic(BlogCategory)


class BlogSchemaPag(Pagination):
    data: List[BlogSchema]

    class Config:
        orm_mode = True


class BlogNewsLetterSchemaPag(Pagination):
    data: List[BlogNewsLetterSchema]

    class Config:
        orm_mode = True



class BlogCategorySchemaPag(Pagination):
    data: List[BlogCategorySchema]

    class Config:
        orm_mode = True