from fastapi import APIRouter
from api.account.views import account 
from api.public.views import public
from api.blog.views import blog
from api.main.views import main


router = APIRouter()
router.include_router(account, prefix='/account', tags=['accounts'])
router.include_router(public, tags=['public'])
router.include_router(main, tags=['main'])
router.include_router(blog, prefix='/blog', tags=['blog'])

