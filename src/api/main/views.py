import time
from datetime import datetime
import json
from fastapi import APIRouter, Depends, HTTPException, status
from models.profiles import Profile, ProfileSkill, ProfileEdu, ProfileJob, ProfileProject, ProfileLang, \
    ProfileMessage
from sqlalchemy import  or_, and_
import models.user as user_model
from fastapi_jwt_auth import AuthJWT
from utils.dep import db,send_sms, strip_tags, redis_q,paginate, paginate_user,get_paginated_list, jsonify_object, get_lang_name
from utils.email import send_email
from schemas.userschema import MessageSchema, UserSchema
from .mainschema import  CreateProfileEduSchema, CreateProfileJobSchema, CreateProfileLangSchema, CreateProfileProjectSchema, CreateProfileSkill, CreateQuestionSchema, EditProfileEduSchema, EditProfileJobSchema, EditProfileLangSchema, EditProfileProjectSchema, EditProfileSkillSchema,\
    EditQuestionSchema, InviteUserSchema, ProfileMessageSchema, CreateAnswerSchema, EditAnswerSchema, SendMessageSchema
from config.config import settings, Templates
from models.user import get_level



ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}

UPLOAD_FOLDER = "uploads"
BUCKET = "networkedng1"


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


main = APIRouter()



def return_id(obj):
    return obj.id


@main.post('/messages/{recipient_id}/', status_code=status.HTTP_201_CREATED, response_model=MessageSchema)
async def send_message(message:SendMessageSchema, recipient_id:int, authorize: AuthJWT = Depends(use_cache=True)):
    """
    View to send message to user,
    required atrr: message:str
            reciprient id : int
            Bearer token : Jwt(str)
    """
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()
    user = db.session.query(user_model.User).filter_by(id=recipient_id).first()
    if user is None:
        raise HTTPException(status_code=404, detail='Message recipient not found')
    if current_user.id == user.id:
        raise HTTPException(status_code=403, detail="You can't send messages to yourself")
    msg = user_model.Message(user_id=current_user.id, recipient_id=recipient_id,
                      body=strip_tags(message.message))
    db.session.add(msg)
    db.session.commit()
    db.session.refresh(msg)
    user.add_notification('unread_message', msg.id, related_id=current_user.id,
                              permanent=True)
    msg = db.session.query(user_model.Message).get(msg.id)
    return jsonify_object(msg)

@main.post('/profile_messages/{recipient_id}/{profile_id}/')
async def PostProfileMessage(message:str, recipient_id:int, profile_id:int, authorize:AuthJWT = Depends(use_cache=True)):
    """
    View to send Professional message to user gig,
    required atrr: message:str
            profile id : int
            Bearer token : Jwt(str)
    """

    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()
    user = db.session.query(user_model.User).filter_by(id=recipient_id).first()
    if user is None:
        raise HTTPException(status_code=404, detail='Recipient not Found')
    profile_instance = db.session.query(Profile).filter(or_(Profile.user == user, Profile.user == current_user)).filter_by(
            id=profile_id).first()
    if profile_instance is None:
        raise HTTPException(status_code=404, detail='Profile not Found')   

    if current_user.id == profile_instance.user_id:
        raise HTTPException(status_code=403, detail="You can't send messages to your gigs")
    
    msg = ProfileMessage(user_id=current_user.id, recipient_id=recipient_id, profile=profile_instance,
                             body=strip_tags(message))
    db.session.add(msg)
    db.session.commit()
    db.session.refresh(msg)
    user.add_notification('unread_professional_message',
                msg.id, related_id=current_user.id, permanent=True)
    msg = db.session.query(ProfileMessage).get(msg.id)
    return await ProfileMessageSchema.from_orm(msg)


@main.get('/messages/{user_id}/{page_id}/', status_code=status.HTTP_200_OK)
async def get_messages(user_id, page_id, authorize: AuthJWT = Depends(use_cache=True)):
    """
    View to Return paginated conversation of the current user with another user instance,
    required atrr: page id:for pagination (int)
            user id : int( Requested user)
            Bearer token : Jwt(str)
    """
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()
    user = db.session.query(user_model.User).filter_by(id=user_id).first
    if user is None:
        raise HTTPException(status_code=404, detail='User not Found')
    for message in current_user.history(user.id):
        if message.recipient_id == current_user.id:
            message.read_at = db.func.now()
            db.session.add(message)
        db.session.commit()
    messages = db.session.query(user_model.Message).order_by(user_model.Message.timestamp.desc()). \
            filter(or_(and_(user_model.user_model.Message.recipient_id == user_id, user_model.user_model.Message.user_id == current_user.id),
                       and_(user_model.Message.recipient_id == current_user.id, user_model.Message.user_id == user_id)))
    messages = paginate(messages, page=page_id, page_size= settings.PAGE_SIZE)
    messages = get_paginated_list(messages)
    return {
            'messages': messages,
            'now': str(datetime.now())
        }

@main.get('/profile_messages/{profile_id}/{user_id}/{page_id}', status_code=status.HTTP_200_OK)
async def GetProfileMessages(profile_id:int, user_id:int, page_id:int, authorize: AuthJWT = Depends(use_cache=True)):
    """
    View to Return Paginated professional message of current user
    object with another user object ,
    required atrr: profile id:int
            user id : int
            page id : int
            Bearer token : Jwt(str)
    """
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()
    user = db.session.query(user_model.User).filter_by(id=user_id).first()
    if user is None:
        raise HTTPException(status_code=404, detail='User not Found')
    for message in current_user.professional_message_history(user.id, profile_id):
        if message.recipient_id == current_user.id:
            db.session.add(message)
            message.read_at = db.func.now()
        db.session.commit()
    messages = db.session.query(ProfileMessage).filter_by(profile_id=profile_id).order_by(ProfileMessage.timestamp.desc()). \
        filter(or_(and_(ProfileMessage.recipient_id == user_id, ProfileMessage.user_id == current_user.id),
                and_(ProfileMessage.recipient_id == current_user.id, ProfileMessage.user_id == user_id)))
    
    messages = paginate(messages,  page=page_id, page_size= settings.PAGE_SIZE)
    
    return {
            'status': 1,
            'messages': messages,
            'now': str(datetime.now())
        }

@main.get('/toggle_follow/{user_id}', status_code=200)
async def ToggleFollow(user_id:int, authorize: AuthJWT = Depends(use_cache=True)):
    """
    View to toggle current user follow / unfollow a user,
    required atrr: 
            user id : int (Requested user object)
            Bearer token : Jwt(str)
    """
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()
    user = db.session.query(user_model.User).filter(user_model.User.id == user_id).first()
    if user is None:
        raise HTTPException(status_code=404, detail='User not Found')   
    if user == current_user:
        raise HTTPException(status_code=403, detail='You cannot follow yourself') 
    if current_user in user.followers:
        current_user.unfollow(user)
        message = "You just un-followed {}".format(user.full_name)
        following = False
        db.session.commit()
    else:
        current_user.follow(user)
        message = "You just followed {}".format(user.full_name)
        following = True
        db.session.commit()
        user.add_notification(name='new_follower', data=message,
                    related_id=current_user.id,
                        permanent=True)
    return {
        'status': 1,
        'message': message,
        'following': following
    }


async def fill_profile(profile_instance):
    skills = [ jsonify_object(skill) for skill in profile_instance.skills]
    education = [jsonify_object(edu, True) for edu in profile_instance.education]
    jobs = [jsonify_object(job, True) for job in profile_instance.jobs]
    languages = [{**jsonify_object(lang), **{'name': get_lang_name(lang.lang)}} for lang in profile_instance.languages]
    projects = [jsonify_object(project, True) for project in profile_instance.projects]
    full_name = profile_instance.full_name
    completeness = profile_instance.completeness
    profile_instance = jsonify_object(profile_instance)
    profile_instance['full_name'] = full_name
    profile_instance['skills'] = skills
    profile_instance['education'] = education
    profile_instance['jobs'] = jobs
    profile_instance['languages'] = languages
    profile_instance['projects'] = projects
    profile_instance['completeness'] = completeness
    return profile_instance

@main.get('/profile/{profile_id}/', status_code=status.HTTP_200_OK)
async def GetProfileInfo(profile_id:int, authorize:AuthJWT=Depends(use_cache=True)):
    
    """
    View to Return Profile instance attributes and reltionships,
    required atrr: profile id :id
            Current user first name (str) and last name (str) 
            Bearer token : Jwt(str)
    """
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()
    profile_instance = db.session.query(Profile).filter_by(user=current_user).filter_by(id=profile_id).first() #filter_by(id=profile_id, first_name=first_name,
                                                                                #last_name=last_name).first()
    if profile_instance is None:
        raise HTTPException(status_code=404, detail='Profile not Found')       
    profile = await fill_profile(profile_instance)
    return {
            "status": 1,
            "profile": profile
    }

@main.post('/profile/{profile_id}/skill/', status_code=201)
async def AddProfileSkill(data:CreateProfileSkill, profile_id:int, authorize:AuthJWT=Depends(use_cache=True)):
    
    """
    View to Add skills to a profile instance,
    query atrr: profile id:str
            Bearer token : Jwt(str)
    """
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()    
    profile_instance = db.session.query(Profile).filter_by(user=current_user).filter_by(id=profile_id).first()
    if profile_instance is None:
        raise HTTPException(status_code=404, detail='User not Found')   
            
    skill = ProfileSkill(profile=profile_instance, **data.dict())
    db.session.add(skill)
    db.session.commit()
    db.session.refresh(skill)
    db.session.refresh(profile_instance)
    profile = await fill_profile(profile_instance)
    return {
            'status': 1,
            'profile': profile
        }

@main.delete('/profile/{profile_id}/skill/delete/{skill_id}/', status_code=200)
async def DeleteProfileSkill(profile_id:int, skill_id:int, authorize:AuthJWT=Depends(use_cache=True)):
    """
    View to Delete a skill from profile instance,
    query atrr: profile id:int
            skill id : int
            Bearer token : Jwt(str)
    """
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()    
    profile_instance = db.session.query(Profile).filter_by(user=current_user).filter_by(id=profile_id).first()
    if profile_instance is None:
        raise HTTPException(status_code=404, detail='Profile not Found')   
            
    skill = db.session.query(ProfileSkill).filter_by(profile=profile_instance).filter_by(id=skill_id).first()
    if skill is None:
        raise HTTPException(status_code=404, detail='Profile Skill not Found')   
            
    db.session.delete(skill)
    db.session.commit()
    db.session.refresh(profile_instance)
    profile = await fill_profile(profile_instance)
    return {
            'status': 1,
            'profile': profile,
            'message':"sucessfully deleted skill from profile"
    }


@main.post('/profile/{profile_id}/skill/edit/{skill_id}/', status_code=201)
async def EditProfileSkill(data:EditProfileSkillSchema, profile_id:int, skill_id:int,authorize:AuthJWT=Depends(use_cache=True)):    
    
    """
    View to Edit  Skill belonging to a profile instance,
    Only current user objects are allowed as others will results in errors
    query atrr: skill id:id
            profile id : int
            Bearer token : Jwt(str)
    """
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()
    profile_instance = db.session.query(Profile).filter_by(user=current_user).filter_by(id=profile_id).first()
    if profile_instance is None:
        raise HTTPException(status_code=404, detail='Profile not Found')   
           
    skill = db.session.query(ProfileSkill).filter_by(profile=profile_instance).filter_by(id=skill_id).first()
    if skill is None:
        raise HTTPException(status_code=404, detail='Profile Skill not Found')   
            
    skill.name = data.name
    skill.profile = profile_instance
    skill.description = data.description
    skill.exp = data.exp
    db.session.add(skill)
    db.session.commit()
    db.session.refresh(skill)
    db.session.refresh(profile_instance)
    profile = await fill_profile(profile_instance)
    return {
            'status': 1,
            'profile': profile
        }

@main.post('/profile/{profile_id}/edu/', status_code=201)
async def AddProfileEdu(data:CreateProfileEduSchema, profile_id:int, authorize:AuthJWT=Depends(use_cache=True)):
    
    """
    View to add Education history to a profile instance,
    query atrr: profiel id:int
            Bearer token : Jwt(str)
    """
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()
    print(current_user.full_name)
    profile_instance = db.session.query(Profile).filter_by(id=profile_id).filter_by(user=current_user).filter_by(user_id = current_user.id).first()
    if profile_instance is None:
        raise HTTPException(status_code=404, detail='Profile not Found')   
           
    data.end_date = None if data.end_date == '' else data.end_date
    edu = ProfileEdu(profile=profile_instance,
                     **data.dict())
    db.session.add(edu)
    db.session.commit()
    db.session.refresh(edu)
    db.session.refresh(profile_instance)
    profile = await fill_profile(profile_instance)
    return {
            'status': 1,
            'profile': profile
    }

@main.post('/profile/{profile_id}/edu/edit/{edu_id}', status_code=201)
async def EditProfileEdu(data:EditProfileEduSchema, profile_id:int, edu_id:int, authorize:AuthJWT=Depends()):
     
    """
    View to Edit Education history to a profile instance,
    query atrr: profile id:int,
            education object id : int
            Bearer token : Jwt(str)
    """
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()    
    profile_instance = db.session.query(Profile).filter_by(user=current_user).filter_by(id=profile_id).first()
    if profile_instance is None:
        raise HTTPException(status_code=404, detail='Profile not Found')            
    edu = db.session.query(ProfileEdu).filter_by(profile=profile_instance).filter_by(id=edu_id).first()
    if edu is None:
        raise HTTPException(status_code=404, detail='Profile Education object not Found')   
            
    data.end_date = None if data.end_date == '' else data.end_date
    edu.school = data.school
    edu.profile = profile_instance
    edu.degree = data.degree
    edu.start_date = data.start_date
    edu.end_date = data.end_date
    db.session.add(edu)
    db.session.commit()
    db.session.refresh(edu)
    db.session.refresh(profile_instance)
    profile = await fill_profile(profile_instance)
    return {
            'status': 1,
            'profile': profile
        }

@main.delete('/profile/{profile_id}/edu/delete/{edu_id}/', status_code=200)
async def DeleteProfileEdu(profile_id:int, edu_id:int, authorize:AuthJWT=Depends(use_cache=True)):
    
     
    """
    View to Delete Education history to a profile instance,
    required atrr: profile id:int,
        education object id : int,
        Bearer token : Jwt(str)
    """
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()    
    profile_instance = db.session.query(Profile).filter_by(user=current_user).filter_by(id=profile_id).first()
    if profile_instance is None:
        raise HTTPException(status_code=404, detail='Profile object not Found')   
            
    edu = db.session.query(ProfileEdu).filter_by(profile=profile_instance).filter_by(id=edu_id).first()
    if edu is None:
        raise HTTPException(status_code=404, detail='Profile Education instance not Found')   
            
    db.session.delete(edu)
    db.session.commit()
    db.session.refresh(profile_instance)
    profile = await fill_profile(profile_instance)
    return {
            'status': 1,
            'profile': profile
        }

@main.post('/profile/{profile_id}/job', status_code=201)
async def AddProfileJob(data:CreateProfileJobSchema, profile_id:int, authorize:AuthJWT=Depends(use_cache=True)):
    
     
    """
    View to add Jb history to a profile instance,
    query atrr: profile id:int
            Bearer token : Jwt(str)
    """
    
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()   
    profile_instance = db.session.query(Profile).filter_by(user=current_user).filter_by(id=profile_id).first()
    if profile_instance is None:
        raise HTTPException(status_code=404, detail='Profile not Found')   
            
    data.end_date = None if data.end_date == '' else data.end_date
    job = ProfileJob(**data.dict(), profile=profile_instance)
    db.session.add(job)
    db.session.commit()
    db.session.refresh(job)
    db.session.refresh(profile_instance)
    profile = await fill_profile(profile_instance)
    return {
            'status': 1,
            'profile': profile
        }

@main.post('/profile/{profile_id}/job/edit/{job_id}', status_code=201)
async def EditProfileJob(data:EditProfileJobSchema, profile_id:int, job_id:int, authorize:AuthJWT=Depends(use_cache=True)):
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()
        
    profile_instance = db.session.query(Profile).filter_by(user=current_user).filter_by(id=profile_id).first()
    
    if profile_instance is None:
        raise HTTPException(status_code=404, detail='Profile not Found')   
            
    data.end_date = None if data.end_date == '' else data.end_date
    job = db.session.query(ProfileJob).filter_by(profile=profile_instance).filter_by(id=job_id).first()
    if job is None:
        raise HTTPException(status_code=404, detail='Profile Job Instance not Found')   
    job.title = data.title
    job.profile = profile_instance
    job.company = data.company
    job.commitment = data.commitment
    job.start_date = data.start_date
    job.end_date = data.end_date
    db.session.add(job)
    db.session.commit()
    db.session.refresh(job)
    db.session.refresh(profile_instance)
    profile = await fill_profile(profile_instance)
    return {
            'status': 1,
            'profile': profile
        }

@main.delete('/profile/{profile_id}/job/delete/{job_id}', status_code=200)
async def DeleteProfileJob(profile_id:int, job_id:int, authorize:AuthJWT=Depends(use_cache=True)):
    
     
    """
    View to Delete Job history to a profile instance,
    required atrr: profile id:int
    Job id : int,
    Bearer token : Jwt(str)
    """
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()    
    profile_instance = db.session.query(Profile).filter_by(user=current_user).filter_by(id=profile_id).first()
    if profile_instance is None:
        raise HTTPException(status_code=404, detail='Proile not Found')    
            
    job = db.session.query(ProfileJob).filter_by(profile=profile_instance).filter_by(id=job_id).first()
    if job is None:
        raise HTTPException(status_code=404, detail='Profile Job Instance not Found')   
            
    db.session.delete(job)
    db.session.commit()
    db.session.refresh(profile_instance)
    profile = await fill_profile(profile_instance)
    return {
            'status': 1,
            'profile': profile
        }

@main.post('/profile/{profile_id}/project', status_code=201)
async def AddProfileProject(data: CreateProfileProjectSchema,profile_id:int, job_id:int, authorize:AuthJWT=Depends(use_cache=True)):
      
    """
    View to add Projects history to a profile instance,
    query atrr: profile id:int,
        Bearer token : Jwt(str)
    """
    
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()    
    profile_instance = db.session.query(Profile).filter_by(user=current_user).filter_by(id=profile_id).first()
    if profile_instance is None:
        raise HTTPException(status_code=404, detail='Proile not Found')  
            
    data.end_date = None if data.end_date== '' else data.end_date
    data.links = json.dumps(data.links)
    project = ProfileProject(**data.dict(), profile=profile_instance
                                 )
    db.session.add(project)
    db.session.commit()
    db.session.refresh(project)
    db.session.refresh(profile_instance)
    profile = await fill_profile(profile_instance)
    return {
            'status': 1,
            'profile': profile
        }

@main.post('/profile/{profile_id}/project/edit/{project_id}', status_code=201)
async def EditProfileProject(data:EditProfileProjectSchema, profile_id:int, project_id:int, authorize:AuthJWT=Depends(use_cache=True)):
    
    """
    View to Edit Project history belonging to a profile instance,
    query atrr: profile id:int
        project id: int
        Bearer token : Jwt(str)
    """
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()    
    profile_instance = db.session.query(Profile).filter_by(user=current_user).filter_by(id=profile_id).first()
    if profile_instance is None:
        raise HTTPException(status_code=404, detail='Proile not Found')   
            
    data.end_date = None if data.end_date == '' else data.end_date
    data.links = json.dumps(data.links)
    project = db.session.query(ProfileProject).filter_by(profile=profile_instance).filter_by(id=project_id).first()
    if project is None:
        raise HTTPException(status_code=404, detail='Project Object not Found')   
            
    project.name = data.name
    project.description = data.description
    project.links = data.links
    project.start_date = data.start_date
    project.end_date = data.end_date
    db.session.add(project)
    db.session.commit()
    db.session.refresh(project)
    db.session.refresh(profile_instance)
    profile = await fill_profile(profile_instance)
    return {
            'status': 1,
            'profile': profile
        }

@main.delete('/profile/{profile_id}/project/delete/{project_id}', status_code=200)
async def  DeleteProfileProject(profile_id:int, project_id:int, authorize:AuthJWT=Depends(use_cache=True)):
    
    """
    View to add Delete a Project history belonging to a profile instance,
    query atrr: profile id:int,
        project id : int
        Bearer token : Jwt(str)
    """
    
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()    
    profile_instance = db.session.query(Profile).filter_by(user=current_user).filter_by(id=profile_id).first()
    if profile_instance is None:
        raise HTTPException(status_code=404, detail='Proile not Found')    
            
    project = db.session.query(ProfileProject).filter_by(profile=profile_instance).filter_by(id=project_id).first()
    if project is None:
        raise HTTPException(status_code=404, detail='Project object not Found')   
           
    db.session.delete(project)
    db.session.commit()
    db.session.refresh(profile_instance)
    profile = await fill_profile(profile_instance)
    return {
            'status': 1,
            'profile': profile
        }

@main.post('/profile/{profile_id}/lang', status_code=201)
async def AddProfileLang(data:CreateProfileLangSchema,profile_id:int, authorize:AuthJWT = Depends(use_cache=True)):
    
     
    """
    View to add Language Object to a profile instance,
    query atrr: profile id:int
            Bearer token : Jwt(str)
    """
    
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()    
    profile_instance = db.session.query(Profile).filter_by(user=current_user).filter_by(id=profile_id).first()
    if profile_instance is None:
        raise HTTPException(status_code=404, detail='Proile not Found')   
           
    lang = ProfileLang(**data.dict(), profile=profile_instance)
    db.session.add(lang)
    db.session.commit()
    db.session.refresh(lang)
    db.session.refresh(profile_instance)
    profile = await fill_profile(profile_instance)
    return {
            'status': 1,
            'profile': profile
        }

@main.post('/profile/{profile_id}/lang/edit/{lang_id}/', status_code=status.HTTP_201_CREATED)
async def EditProfileLang(data:EditProfileLangSchema,profile_id:int, lang_id:int, authorize:AuthJWT = Depends(use_cache=True) ):
    
     
    """
    View to Edit Language Object belonging to a profile instance,
    required atrr: profile id:int,
        language object id : int,
            Bearer token : Jwt(str)
    """
    
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()
    profile_instance = Profile.query.filter_by(user=current_user).filter_by(id=profile_id).first()
    if profile_instance is None:
        raise HTTPException(status_code=404, detail='Profile not Found')   
            
    lang = ProfileLang.query.filter_by(profile=profile_instance).filter_by(id=lang_id).first()
    if lang is None:
        raise HTTPException(status_code=404, detail='Profile Language not Found')   

    lang.lang = data.lang
    lang.level = data.level
    db.session.add(lang)
    db.session.commit()
    db.session.refresh(lang)
    db.session.refresh(profile_instance)
    profile = fill_profile(profile_instance)
    return {
            'status': 1,
            'profile': await profile
        }

'/profile/<int:profile_id>/lang/delete/<int:lang_id>'

@main.delete('/profile/{profile_id}/lang/edit/{lang_id}', status_code=status.HTTP_200_OK)
async def DeleteProfileLang(profile_id:int, lang_id:int, authorize: AuthJWT= Depends(use_cache=True)):
    
     
    """
    View to Delete Language Object belonging to a profile instance,
    required atrr: profile id:int,
        lanaguage object id : int,
            Bearer token : Jwt(str)
    """
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()    
    profile_instance = db.session.query(Profile).filter_by(user=current_user).filter_by(id=profile_id).first()
    if profile_instance is None:
        raise HTTPException(status_code=404, detail='Proile not Found')   
    lang = db.session.query(ProfileLang).filter_by(profile=profile_instance).filter_by(id=lang_id).first()
    if lang is None:
        raise HTTPException(status_code=404, detail='Proile Language not Found')   
            
    db.session.delete(lang)
    db.session.commit()
    db.session.refresh(profile_instance)
    profile = fill_profile(profile_instance)
    return {
            'status': 1,
            'profile': await profile
        }


###User Profile Section Ends here




##Main Section begins




"""@main.route('/search')
@login_required
def search():
    query = request.args.get('query')
    page = request.args.get('page')
    search_type = request.args.get('type')

    query = query if query is not None else 'All'
    page = page if page is not None else 1
    try:
        page = int(page)
    except:
        page = 1
    search_type = search_type if search_type is not None else ''
    results = []
    if search_type == '':
        user_results = User.query.whooshee_search(query, order_by_relevance=0).all()
        questions_results = Question.query.whooshee_search(query, order_by_relevance=0).all()
        job_results = Job.query.whooshee_search(query, order_by_relevance=0).all()
        post_results = Post.query.whooshee_search(query, order_by_relevance=0).all()
        organisations_results = Organisation.query.whooshee_search(query, order_by_relevance=0).all()

        job_results_count = Job.query.whooshee_search(query, order_by_relevance=0).count()
        user_results_count = User.query.whooshee_search(query, order_by_relevance=0).count()
        questions_results_count = Question.query.whooshee_search(query, order_by_relevance=0).count()
        post_results_count = Post.query.whooshee_search(query, order_by_relevance=0).count()
        organisations_results_count = Organisation.query.whooshee_search(query, order_by_relevance=0).count()

        all_results = job_results + user_results + questions_results + post_results + organisations_results
        all_count = job_results_count + user_results_count + questions_results_count + post_results_count + organisations_results_count
        results = all_results[:40]
        paginator = Pagination(items=results, page=page, per_page=40, query=None, total=all_count)
        results = paginator

    elif search_type == 'people':
        results = User.query.whooshee_search(query, order_by_relevance=-1).paginate(page, per_page=40)
        # results = sorted(user_results, key=operator.attrgetter("score"))
    elif search_type == 'jobs':
        results = Job.query.whooshee_search(query, order_by_relevance=-1).paginate(page, per_page=40)
        # results = sorted(job_results, key=operator.attrgetter("score"))
    elif search_type == 'questions':
        results = Question.query.whooshee_search(query, order_by_relevance=-1).paginate(page, per_page=40)
    elif search_type == 'post':
        results = Post.query.whooshee_search(query, order_by_relevance=-1).paginate(page, per_page=40)
    elif search_type == 'organisations':
        results = Organisation.query.whooshee_search(query, order_by_relevance=-1).paginate(page, per_page=40)

    return render_template("main/search_results.html", query=query, search_type=search_type, results=results)

"""""



"""@main.route('/marketplace')
@login_required
def marketplace():
    user_id = current_user.id
    user = User.query.filter_by(id=user_id).first()
    photo = Photo.query.filter_by(user_id=user_id).first()
    if photo is None:
        user_instance = {
            'email': user.email,
            'phone':user.mobile_phone,
            'first_name':user.first_name,
            'last_name': user.last_name,
            'area_code':user.area_code,
            'country':user.country,
            'profession':user.profession,
            'city': user.city,
            'image_filename': 'demo.png',
            'image_url': user.get_photo()
         }
    else:
        user_instance = {
            'email': user.email,
            'phone':user.mobile_phone,
            'first_name':user.first_name,
            'last_name': user.last_name,
            'area_code':user.area_code,
            'country':user.country,
            'profession':user.profession,
            'city': user.city,
            'image_filename': photo.image_filename,
            'image_url': photo.image_url
         }   
    #requests.post('https://marketplace.networked.ng/account/api/v1/register', json=user_instance)      
    get_queue().enqueue(register_to_marketplace, data=user_instance)
    return redirect('https://marketplace.networked.ng')
"""



@main.get('/members/page/{page_id}/', status_code=200)
async def select_section(page_id:int, authorize: AuthJWT= Depends(use_cache=True)):
    """
    Endpoint to list all registered proffessional / Users
    page:integer for pagination
    """
    start = time.time()
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()    
    paginated = paginate_user(db.session.query(user_model.User).filter(user_model.User.id != current_user.id).order_by(user_model.User.id.desc()), page=page_id, page_size= settings.PAGE_SIZE)
    end = time.time() - start
    print(end)
    return await paginated


"""@main.route('/members/', defaults={'page': 1})
@main.route('/members/page/<int:page>', methods=['GET'])
def members(page):
    paginated = User.query.filter(User.id != current_user.id).order_by(User.id.desc()).paginate(page, per_page=25)
    return render_template('main/members.html', paginated=paginated)
"""

## if you change profile to user on the routes it will share posts and other details.
##I made this change from user to profile so as to get the profile name to be on the url


@main.get('/profile/{user_id}/{full_name}/{active}/page/{page}')
async def user(user_id:int, full_name:str, active:str, page:int, authorize:AuthJWT=Depends(use_cache=True)):
    """
    Endpoint to return a user attributes, posts question for computing a user timeline,
    active: string representing either user posts instance or questions e.g
    https://example.com/profile/1/test user/posts/page/1/, for posts and 
    https://example.com/profile/1/test user/questions/page/1/ for questions
    """
    
    authorize.jwt_required()
    user = db.session.query(user_model.User).filter(user_model.User.id == user_id, user_model.User.full_name == full_name).first()
    if user is None:
        raise HTTPException(status_code=404, detail='User not found')
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()
    if user == current_user:
        raise HTTPException(status_code=403, detail="Forbidden")
    if active == 'posts':
        items = paginate(user.posts, page, page_size=10)
        return dict(active=active, items=[jsonify_object(i) for i in items])
    elif active == 'questions':
        items = paginate(user.questions, page, page_size=10)
        return dict(active=active, items=[jsonify_object(i) for i in items])
    else:
        items = []
    return dict(active=active, items=items)




@main.get('/public-profile/{user_id}/{full_name}/', status_code=200)
async def public_user_profile(user_id:int, full_name:str):
    """Provide a public HTML page with all details on a given user """
   
    profile_instance = db.session.query(Profile).filter_by(user_id=user_id, full_name=full_name).first()
    if not profile_instance:
        raise HTTPException(detail="User has no professional profile", status_code=404)
    user = db.session.query(user_model.User).filter_by(id = profile_instance.user.id).first()
    if user is None:
        raise HTTPException(detail="User not found", status_code=404)  
    profile = jsonify_object(profile_instance, only_date=True)
    user = UserSchema.from_orm(user)
    return {'profile':profile, 'user':user}
   


"""@main.route('/public/<full_name>.pdf')
def resume_pdf(full_name):
    user = db.session.query(User).filter(User.full_name == full_name).first()
    resume = db.session.query(Resume).filter(Resume.id == user.id).first()
    extra = db.session.query(Extra).filter(Extra.id == user.id).first()
    user_id = Photo.user_id
    photo = Photo.query.filter_by(id=user_id).limit(1).all()
    # Make a PDF straight from HTML in a string.
    html = render_template('public/public_profile.html', full_name=User.full_name, user=user, resume=resume,
                           extra=extra, photo=photo, )
    return render_pdf(HTML(string=html))"""



@main.get("/followers/{user_id}/{page}/", status_code=200)
async def followers(user_id:int, page:int = 1, authorize:AuthJWT=Depends(use_cache=True)):
    """
    An endpoint returning all users following a given user instance,
    user_id: integer , page: integer for pagination
    """ 
    authorize.jwt_required()
    user_instance = db.session.query(user_model.User).get(user_id)
    followers_users = await paginate_user(user_instance.followers, page=page, page_size=settings.PAGE_SIZE)
    return followers_users


@main.get("/following/{user_id}/{page}/", status_code=200)
async def following(user_id:int, page:int = 1, authorize:AuthJWT=Depends(use_cache=True)):
    """
    An endpoint returning all users followed by a  given user instance,
    user_id: integer , page: integer for pagination
    """ 
    authorize.jwt_required()
    user_instance = db.session.query(user_model.User).get(user_id)
    followers_users = await paginate_user(user_instance.followed, page=page, page_size=settings.PAGE_SIZE)
    return followers_users


"""@main.route('/photo/upload', methods=['GET', 'POST'])
@login_required
def photo_upload():
    ''' check if photo already exist, if it does, send to homepage. Avoid duplicate upload here'''
    check_photo_exist = db.session.query(Photo).filter(Photo.user_id == current_user.id).count()
    if check_photo_exist >= 1:
        pass
        # return redirect(url_for('main.index'))
    form = PhotoForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            image_filename = images.save(request.files['photo'])
            image_url = images.url(image_filename)
            picture_photo = Photo.query.filter_by(user_id=current_user.id).first()
            if not picture_photo:
                picture_photo = Photo(
                    image_filename=image_filename,
                    image_url=image_url,
                    user_id=current_user.id,
                )
            else:
                picture_photo.image_filename = image_filename
                picture_photo.image_url = image_url
            db.session.add(picture_photo)
            db.session.commit()
            flash("Image saved.")
            return redirect(url_for('main.index'))
        else:
            flash('ERROR! Photo was not saved.', 'error')
    return render_template('main/upload.html', form=form)
"""

@main.post('/invite-colleague', status_code=201 )
async def invite_user(data:InviteUserSchema, authorize:AuthJWT=Depends(use_cache=True)):
    """Invites a new user to create an account and set their own password."""
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()    
    phone_instance = db.session.query(user_model.User).filter_by(mobile_phone=data.mobile_phone).first()
    if phone_instance is not None:
        raise HTTPException(detail='Phone Number belongs to a registered user', status_code=400)
    email_instance = db.session.query(user_model.User).filter_by(email=data.email).first()
    if email_instance is not None:
        raise HTTPException(detail='Email belongs to a registered user', status_code=400)
    user = user_model.User(
            invited_by=current_user.full_name,
            first_name=data.first_name,
            last_name=data.last_name,
            area_code=data.area_code,
            mobile_phone=data.mobile_phone
        )
    db.session.add(user)
    db.session.commit()
    token = user.generate_email_confirmation_token()
    invite_link = f"{data.link}/{token}/"
    area_code = str(data.area_code.data)
    area_code = area_code.replace(' ', '')
    phone_number = str(data.mobile_phone.data)
    phone_number = phone_number.replace(' ', '')
    if str(area_code)[0] != '+':
        area_code = '+' + str(area_code)
    body = {'user':current_user.full_name,
                    'invite_link':invite_link}
    template = Templates.get_template("/account/email/invite.jinja2")
    rendered = template.render(**body)    
    redis_q.enqueue(
            send_email,
            recipient=data.email,
            subject='You Are Invited To Join',
            template=rendered,
            body=body
            )  
    redis_q.enqueue(  
    send_sms,
    message=f'Invitation to networked.ng by %s. Click link to complete registration : {invite_link}' % current_user.full_name,
    to_phone=str(area_code) + str(phone_number))
    return dict(message='User {} successfully invited'.format(user.full_name),
              status='success')


@main.get('/conversations/{page}/',)
async def conversations(page: int, authorize:AuthJWT=Depends(use_cache=True)):

    """
    Endpoint returning a given user paginated conversations, page: integer 
    """
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()    
    current_user.last_message_read_time = datetime.utcnow()
    db.session.commit()
    messages = current_user.messages_received.order_by(
        user_model.Message.timestamp.desc())
    paginated = paginate(messages,     
        page=page, page_size=settings.PAGE_SIZE)
    conversations = db.session.query(user_model.Message).filter(
        or_(user_model.Message.user_id == current_user.id, user_model.Message.recipient_id == current_user.id)).all()
    user_ids = [conversation.user_id for conversation in conversations] + [conversation.recipient_id for conversation in
                                                                           conversations]
    user_ids = list(set(user_ids))

    if current_user.id in user_ids:
        user_ids.remove(current_user.id)
    users = db.session.query(user_model.User).filter(user_model.User.id.in_(user_ids))
    users = await paginate_user(users,page=page)
    return {'messages':paginated, 'users':users}


@main.get('/professional/conversations/{page}/')
async def professional_conversations(page: int, authorize:AuthJWT=Depends(use_cache=True)):
    """
    Endpoint returning paginated list of professional messages or chats by a given user
    """

    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()    
    current_user.last_message_read_time = datetime.utcnow()
    db.session.commit()
    conversations = db.session.query(ProfileMessage).filter(
        or_(ProfileMessage.user_id == current_user.id, ProfileMessage.recipient_id == current_user.id)).with_entities(
        ProfileMessage.user_id, ProfileMessage.recipient_id, ProfileMessage.profile_id).distinct().all()
    
    convs = [{
        'sender': return_id(db.session.query(user_model.User).get(conv[0])),
        'recipient': return_id(db.session.query(user_model.User).get(conv[1])),
        'message': jsonify_object(ProfileMessage.query.filter_by(profile_id=conv[2]).filter_by(user_id=conv[0]).filter_by(
            recipient_id=conv[1]).order_by(ProfileMessage.created_at.desc()).first())
    } for conv in conversations]
    convs = sorted(convs, key=lambda item: item["message"]["created_at"])
    convs.reverse()
    filtered_convs = []
    for i in convs:
        if i not in filtered_convs and {"sender": i["recipient"], "recipient": i["sender"],
                                        "message": ProfileMessage.query.filter_by(
                                                profile_id=i["message"]['profile_id']).filter_by(
                                                user_id=i["recipient"]).filter_by(
                                                recipient_id=i["sender"]).order_by(
                                                ProfileMessage.created_at.desc()).first()} not in filtered_convs:
            filtered_convs.append(i)

    all_results = filtered_convs
    print(all_results)
    results = all_results[(page - 40) * 1:page * 40]
    paginator = paginate(results, page=page, page_size=40)
    return dict(results= paginator)


@main.post('/question/{question_id}/edit', status_code=201)
async def edit_question(data:EditQuestionSchema, question_id, authorize:AuthJWT=Depends(use_cache=True)):
    """
    Endpoint to edit questions posted by a given user(an instance of the current user on the bearer token)
    """
    
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()
    question = db.session.query(user_model.Question).filter_by(user_id=current_user.id).filter_by(id=question_id).first()
    if question is None:
        raise HTTPException(detail="Question not found", status_code=404)
    if data.title:    
        question.title = data.title
    if data.description:
        question.description = data.description
    db.session.add(question)
    db.session.commit()
    return dict(status='success', message='Edited successfully', question=jsonify_object(question))



@main.delete('/question/{question_id}/delete')
async def delete_question(question_id, authorize:AuthJWT=Depends(use_cache=True)):
    """
    Endpoint to delete a question instance posted by the current user, question id: integer
    """
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()
    question = db.session.query(user_model.Question).filter_by(user_id=current_user.id).filter_by(id=question_id).first()
    if question is None:
        raise HTTPException(detail="Question not found", status_code=404)
    db.session.delete(question)
    db.session.commit()
    return dict(status="success", message='deleted successfully')


@main.post('/questions', status_code=201)
async def question(data:CreateQuestionSchema,authorize:AuthJWT=Depends(use_cache=True)):
    """
    Route for creating questions, created objects holds relations to the current user
    """
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()
    posts = user_model.Question(
                user_id=current_user.id,
                author=current_user.full_name,
                level=1,
                **data.dict()
    )
    current_user.kw_seeker_points = current_user.kw_seeker_points + 1
    current_user.kw_seeker_badge = get_level(current_user.kw_seeker_points) + " Kw Seeker"
    db.session.add(posts)
    db.session.commit()
    return dict(status='success', message='posted', question= jsonify_object(posts))


@main.get('/questions/list/all/{page}')
async def questions_list(page:int, authorize:AuthJWT=Depends(use_cache=True)):
    """
    Endpoint to return all active questions 
    """
    authorize.jwt_required()
    questions = db.session.query(user_model.Question).filter(user_model.Question.timestamp != None).all()
    paginated = paginate(questions, page=page)
    return paginated


@main.get('/question/{question_id}/{title}/')
async def question_details(question_id:int, title:str):
    """Provide HTML page with all details on a given question.
    """
    question = db.session.query(user_model.Question).filter_by(id=question_id).filter_by(title=title).first()
    if question is None:
        raise HTTPException(detail="Question not found", status_code=404)
    return dict(question=jsonify_object(question))

@main.post('/question/answer/{answer_id}/edit/', status_code=201)
async def edit_answer(data:EditAnswerSchema,answer_id:int, authorize:AuthJWT=Depends(use_cache=True)):
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()
    answer = db.session.query(user_model.Answer).filter_by(user_id=current_user.id).filter_by(id=answer_id).first_or_404()
    question = answer.question
    body = data.reply
    answer.body = body
    db.session.add(answer)
    db.session.commit()
    return dict(status='success', message="Edited Successfully.", answer=jsonify_object(answer), question=jsonify_object(question))


@main.delete('/question/answer/{answer_id}/delete' , status_code=200)
async def delete_answer(answer_id:int, authorize:AuthJWT=Depends(use_cache=True)):
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()
    answer = db.session.query(user_model.Answer).filter_by(user_id=current_user.id).filter_by(id=answer_id).first()
    question = answer.question
    db.session.delete(answer)
    db.session.commit()
    return dict( status='success', message="Deleted Successfully.", question=jsonify_object(question))


@main.post('/question/answer', status_code=201 )
async def parent_answers(data:CreateAnswerSchema, authorize:AuthJWT=Depends(use_cache=True)):
    """
    Endpoint to create an answer to an active question
    """
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()
    question = db.session.query(user_model.Question).filter_by(id=data.question_id).first()
    if question is None:
        raise HTTPException(detail="Question not found", status_code=404)
    photo = current_user.get_photo()
    answers = user_model.Answer(user_id=current_user.id,
                author=current_user.full_name,
                question_id=question.id,
                body=data.reply,
        )
    #if data.parent_id!= '0':
        #answers.parent_id = data.parent_id
    if photo:
        answers.image_url = photo
    current_user.kw_builder_points = current_user.kw_builder_points + 1
    current_user.kw_builder_badge = get_level(current_user.kw_builder_points) + " Kw Builder"
    db.session.add(answers)
    db.session.commit()
    question.user.add_notification(name='answer', data=data.reply, related_id=question.id)
    return dict(status='success', message='Answer posted successfully', question=jsonify_object(question))#, answer=jsonify_object(answers))


@main.get('/notification/read/{notification_id}')
async def read_notification(notification_id:int, authorize:AuthJWT=Depends(use_cache=True)):
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()
    notification = current_user.notifications.filter_by(id=notification_id).first()
    if notification is None:
        raise HTTPException(detail="Notification not found", status_code=404)
    notification.read = True
    db.session.add(notification)
    db.session.commit()
    """if 'unread_message' in notification.name:
        user = db.session.query(user_model.User).filter_by(id=notification.related_id).first_or_404()
        link = url_for('main.send_message', recipient=user.id, full_name=user.full_name)
    if 'unread_professional_message' in notification.name:
        msg = ProfileMessage.query.filter_by(id=json.loads(notification.payload_json)['message']).first_or_404()
        sender = msg.user
        recipient = msg.recipient
        profile_owner = msg.profile.user
        if profile_owner == current_user:
            link = url_for('main.send_professional_message', profile_id=msg.profile.id, recipient=sender.id,
                           full_name=recipient.full_name)
        else:
            link = url_for('employer.hire', profile_id=msg.profile.id)

    elif 'post_likes' in notification.name:
        user = User.query.filter_by(id=notification.related_id).first_or_404()
        post = Post.query.filter_by(id=notification.related_id).first_or_404()
        link = url_for('post.view_post', post_id=post.id)
    elif 'post_replies' in notification.name:
        user = User.query.filter_by(id=notification.related_id).first_or_404()
        post = Post.query.filter_by(id=notification.related_id).first_or_404()
        link = url_for('post.view_post', post_id=post.id)
    elif 'answer' in notification.name:
        user = User.query.filter_by(id=notification.related_id).first_or_404()
        question = Question.query.filter_by(id=notification.related_id).first_or_404()
        link = url_for('main.question_details', question_id=question.id, title=question.title)
    elif 'new_follower' in notification.name:
        user = User.query.filter_by(id=notification.related_id).first_or_404()
        link = url_for('main.user', id=user.id, full_name=user.full_name)
    elif 'new_post_of_followers' in notification.name:
        post = Post.query.filter_by(id=json.loads(notification.payload_json)['post']).first()
        link = url_for('post.view_post', post_id=post.id)
    elif 'new_job' in notification.name:
        job = Job.query.filter_by(id=json.loads(notification.payload_json)['job']).first()
        link = url_for('jobs.job_details', position_id=job.id, position_title=job.position_title,
                       position_city=job.position_city, position_state=job.position_state,
                       position_country=job.position_country)

    return redirect(link)"""
    return dict(message='Notification read successfully', status="Success")


@main.get('/notifications/count')
async def notifications_count(authorize:AuthJWT=Depends(use_cache=True)):
    """
    Endpoint to keep track of new notifications and messages, values returned here represent unread messages only
    """
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()
    notifications = db.session.query(user_model.Notification).filter_by(read=False).filter_by(user_id=current_user.id).count()
    messages = current_user.new_messages()
    return {
        'status': 1,
        'notifications': notifications,
        'messages': messages
    }


@main.get('/notifications')
async def notifications(authorize:AuthJWT=Depends(use_cache=True)):

    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()
    notifications = current_user.notifications.all()
    parsed_notifications = []
    for notification in notifications:
        parsed_notifications.append(notification.parsed())
    parsed_notifications = sorted(parsed_notifications, key=lambda i: i['time'])
    parsed_notifications.reverse()
    parsed_notifications = parsed_notifications[0:15]
    return dict(notifications=[i for i in parsed_notifications])


@main.get('/notifications/more/{count}')
async def more_notifications(count:int, authorize:AuthJWT=Depends(use_cache=True)):
    """
    Endpoint to query for more notifications, example if you need 300 notification
    query will be notifications/more/300, if the number is adeqeute it returns else a 404
    """
    authorize.jwt_required()
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()
    notifications = current_user.notifications.all()
    parsed_notifications = []
    for notification in notifications:
        parsed_notifications.append(notification.parsed())
    parsed_notifications = sorted(parsed_notifications, key=lambda i: i['time'])
    parsed_notifications.reverse()
    if count == 0:
        parsed_notifications = parsed_notifications[0:15]
    elif count >= len(parsed_notifications):
        return dict(status='fail', message="No more Notifications"), 404
    else:
        parsed_notifications = parsed_notifications[count:count + 15]
    return dict(notifications=[i for i in parsed_notifications]), 200




