from pydantic import BaseModel, EmailStr, HttpUrl
from typing import Optional
from schemas.profileschema import *
from pydantic_sqlalchemy import sqlalchemy_to_pydantic
    

ProfileMessagePydantic = sqlalchemy_to_pydantic(ProfileMessage)

class ProfileMessageSchema(ProfileMessagePydantic):
    created_at: str

    class Config:
        orm_mode=True

class CreateProfileLangSchema(BaseModel):
    lang: str
    level:str

class EditProfileLangSchema(BaseModel):
    lang: Optional[str]
    level:Optional[str]

class CreateProfileSkill(BaseModel):
    name: str
    description:str
    exp: str

class EditProfileSkillSchema(BaseModel):
    name: Optional[str]
    description: Optional[str]
    exp: Optional[str]

class CreateProfileEduSchema(BaseModel):
    school: str
    degree: str
    start_date:str
    end_date: str = None

class EditProfileEduSchema(BaseModel):
    school: Optional[str]
    degree: Optional[str]
    start_date: Optional[str]
    end_date: Optional[str] = None


class CreateProfileJobSchema(BaseModel):
    title : str
    company:str
    commitment:str
    start_date: str
    end_date: str = None

class EditProfileJobSchema(BaseModel):
    title : Optional[str]
    company:Optional[str]
    commitment:Optional[str]
    start_date: Optional[datetime]
    end_date: Optional[datetime] = None


class CreateProfileProjectSchema(BaseModel):
    name:str
    description:str
    links:Optional[HttpUrl]
    start_date: datetime
    end_date: datetime = None


class EditProfileProjectSchema(BaseModel):
    name:Optional[str]
    description:Optional[str]
    links:Optional[HttpUrl]
    start_date: Optional[datetime]
    end_date: Optional[datetime] = None
    






class InviteUserSchema(BaseModel):
    first_name : str
    last_name : str
    email: EmailStr
    mobile_phone:int
    area_code:str
    link:HttpUrl


class CreateQuestionSchema(BaseModel):
    title:str
    description:str


class EditQuestionSchema(BaseModel):
    title:Optional[str]
    description:Optional[str]    

class CreateAnswerSchema(BaseModel):
    reply:str
    question_id:int
    parent_id:Optional[int]


class EditAnswerSchema(BaseModel):
    reply:str 


class SendMessageSchema(BaseModel):
    message: str