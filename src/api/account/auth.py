from pydantic import BaseModel, EmailStr, HttpUrl
from typing import Optional, Text
import schemas.userschema as user_model

class Token(BaseModel):
    access_token: str
    token_type: str
    refresh_token: str


    
   
class TokenUser(Token):
    user_status: str
    role_name: str

class TokenData(BaseModel):
    mobile_phone: int = None


class Image(BaseModel):
    url: str
    name: str
        
class Register(BaseModel):
    mobile_phone: int
    password_hash: str
    first_name:str
    last_name:str
    area_code: str
    gender: str
    email:EmailStr

class UpdateAccount(BaseModel):
    first_name:Optional[str]
    last_name:Optional[str]
    area_code: Optional[str]
    gender: Optional[str]
    summary: Text
    city: Optional[str]
    state: Optional[str]
    country: Optional[str]
    profession: Optional[str]




class CreateProfile(BaseModel):
    title:str
    header: str
    commitment: str
    type_of_work: str


class UpdateProfile(BaseModel):
    title:Optional[str]
    header: Optional[str]
    commitment: Optional[str]
    type_of_work: Optional[str]

class ForgotPassword(BaseModel):
    mobile_phone: int
    url: HttpUrl


class ChangePassword(BaseModel):
    mobile_phone: int
    password: str

    

class ChangeEmailRequest(BaseModel):
    email: EmailStr
    password: str
    link: HttpUrl

class ChangeEmail(BaseModel):
    token : str

class OTPToken(BaseModel):
    token : int    