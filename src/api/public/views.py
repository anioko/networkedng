
from fastapi import APIRouter, HTTPException
from schemas.userschema import EditableSchema
from schemas.userschema import ContactSchema
from crud.public import add_contact_message, get_editableobj


public = APIRouter()

@public.get("/about", response_model=EditableSchema)
async def about(
):
    """Async route to handle return an instance of part class queried by id"""
    part_object = await get_editableobj(editor_name='about')
    if part_object is None:
        raise HTTPException(status_code=404, detail="Data does not exist")
    return part_object


@public.get("/terms", response_model=EditableSchema)
async def terms(
):
    """Async route to handle return an instance of part class queried by id"""
    part_object = await get_editableobj(editor_name='terms')
    if part_object is None:
        raise HTTPException(status_code=404, detail="Data does not exist")
    return part_object


@public.get("/faq", response_model=EditableSchema)
async def faq(
):
    """Async route to handle return an instance of part class queried by id"""
    part_object = await get_editableobj(editor_name='faq')
    if part_object is None:
        raise HTTPException(status_code=404, detail="Data does not exist")
    return part_object


@public.get("/privacy", response_model=EditableSchema)
async def privacy(
):
    """Async route to handle return an instance of part class queried by id"""
    part_object = await get_editableobj(editor_name='privacy')
    if part_object is None:
        raise HTTPException(status_code=404, detail="Data does not exist")
    return part_object

@public.post("/contact/", response_model=ContactSchema, status_code=201)
async def contact(message: ContactSchema):
    return await add_contact_message(message,)
    