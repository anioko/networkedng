from fastapi import APIRouter, HTTPException, APIRouter, Depends
from typing import List
from fastapi_jwt_auth import AuthJWT
from models.blog import BlogPost, BlogCategory, BlogTag, BlogComment, BlogNewsLetter
from schemas.blogschema import BlogCategorySchemaPag,BlogNewsLetterSchemaPag, BlogSchemaPag, BlogSchema
from utils.dep import db, jsonify_object, paginate, get_paginated_list
from .blogschema import NewsletterSchema, BlogCommentSchema
import models.user as user_model

blog = APIRouter()



@blog.get("/{page}/")
async def index(page:int):
    """
    Blog Index Page with pagination, page:int 
    """
    posts = db.session.query(BlogPost).order_by(BlogPost.created_at.desc()).all()
    posts = paginate(posts, page=page)
    categories = db.session.query(BlogCategory).order_by(BlogCategory.is_featured.desc()).order_by(BlogCategory.order.desc()).limit(10).all()
    tags = db.session.query(BlogTag).order_by(BlogTag.created_at.desc()).limit(10).all()
    cat_objects=await [jsonify_object(i) for i in categories]
    posts_object=await [jsonify_object(i) for i in posts.items]
    tags= await [jsonify_object(tag) for tag in tags]
    return dict(posts=posts_object, categories=cat_objects, tags=tags)


@blog.get('/categories/{page>}/')
async def blog_categories(page:int):
    """
    Blog Category index page with pagination, page:int
    """
    categories = db.session.query(BlogCategory).query.order_by(BlogCategory.is_featured.desc()).order_by(BlogCategory.order.desc()).all()
    categories = paginate(categories,page=page)
    if categories:
        result = await [jsonify_object(i) for i in categories]
        return dict(categories=result)


@blog.get('/category/{category_id}/')
async def blog_category(category_id:int, page:int):
    """
    Blog category instance detail page, category id: integer, page:integer
    """
    category = db.session.query(BlogCategory).get(category_id)
    posts = db.session.query(BlogPost).filter(BlogPost.categories.any(id=category.id)).order_by(BlogPost.created_at.desc()).all()
    category=paginate(category, page=page)
    if category and posts:
        post_result = await [jsonify_object(i) for i in posts.items ]
        cat_result = jsonify_object(category)
        return dict(posts=post_result, category=cat_result), 200
    else:
        raise HTTPException(detail="Blog Category not found", status_code=404)   





@blog.get('/tag/{tag_id}/{page}')
async def blog_tag(tag_id, page):
    """
    Blog Tag instance detail page with pagination returning all related objects
    , tag id : integer, page : integer 
    """
    tag = db.session.query(BlogTag).query.get(tag_id)
    posts = db.session.query(BlogPost).filter(BlogPost.tags.any(id=tag.id)).order_by(BlogPost.created_at.desc()).all()
    posts = paginate(posts,page=page, per_page=40)
    if tag and posts:
        post_result = await [jsonify_object(i) for i in posts ]
        tag = jsonify_object(tag)
        return dict(posts=post_result, tag=tag), 200
    else:
        raise HTTPException(detail="Tag not found", status_code=404)    




@blog.route('/article/{article_id}')
async def blog_article(article_id:int):
    """
    Blog article instance detail page, return related objects,
    article id : integer. 404 if not found
    """
    post = db.session.query(BlogPost).get(article_id)
    categories = post.categories
    tags = post.tags
    if post and categories and tags:
        post_result = jsonify_object(post)
        cat_result = await [jsonify_object(i) for i in categories]
        tag_result = await [jsonify_object(i) for i in tags ]   
        return dict(post=post_result, tags=tag_result, categories=cat_result), 200
    else:
        raise HTTPException(status_code=404, detail="Article not found")    


@blog.route('/article/{article_id}/comment', methods=['POST'])
async def add_comment(article_id:int, data:BlogCommentSchema,authorize:AuthJWT= Depends(use_cache=True)):
    """
    Protected blog comment endpoint for logged in users only to submit comments, 
    article_id : integer , parent id on the schema represent an instance of nested comments
    """
    
    authorize.jwt_required()
    post = db.session.query(BlogPost).get(article_id)
    if post is None:
        raise HTTPException(detail="Post not found", status_code=404)
    current_user = db.session.query(user_model.User).filter_by(mobile_phone=authorize.get_jwt_subject()).first()
    parent_id = data.parent_id
    if parent_id and parent_id != '0' and parent_id != 0:
        comment = BlogComment(
            post_id=post.id,
            text=data.text,
            user_id=current_user.id,
            parent_id=parent_id
        )
    else:
        comment = BlogComment(
            post_id=post.id,
            text=data.text,
            user_id=current_user.id,
        )
    db.session.add(comment)
    db.session.commit()
    return dict(status='sucess', message="comment added successfuly", comment = jsonify_object(comment)), 201



@blog.post('/sub', status_code=201)
async def subscribe(data:NewsletterSchema):
    """
    Blog Newsletter subscription page
    """
    email = data.email
    if db.session.query(BlogNewsLetter).filter_by(email=email).first():
        raise HTTPException(status_code=400, detail='already subscribed to newsletter')
    sub = BlogNewsLetter(
        email=email
    )
    db.session.add(sub)
    db.session.commit()
    return dict(status='success', message="successfully subscried to newsletter")

