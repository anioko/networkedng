import email
from typing import Any, Optional
from pydantic import EmailStr, BaseModel




class NewsletterSchema(BaseModel):
    email:EmailStr

class BlogCommentSchema(BaseModel):
    text:str
    parent_id:Optional[int]    