from functools import wraps
from fastapi import HTTPException
from utils.security import get_current_user as current_user
from models.user import Permission

def permission_required(permission):
    """Restrict a view to users with the given permission."""

    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            if not current_user.can(permission):
                raise HTTPException(status_code=403)
            return f(*args, **kwargs)

        return decorated_function

    return decorator



def admin_required(f):
    return permission_required(Permission.ADMINISTER)(f)

def marketer_required(f):
    """Restrict a view to users with the given permission."""
    @wraps(f)
    def wrapper(*args, **kwargs):
        if not current_user.is_admin or not current_user.is_marketer:
            raise HTTPException(status_code=403)
        return f(*args, **kwargs)
    return wrapper








