import os
import logging
import os.path
from aiobotocore.session import get_session
from uuid import uuid4
from time import time
from os import system
from PIL import Image
from typing import List, Optional
import aiofiles
from starlette.datastructures import UploadFile
from utils.dep import FileExtNotAllowed, FileMaxSizeLimit
from config.config import settings
from fastapi import HTTPException 
import io



logger = logging.getLogger(__name__)
  
ALLOWED_EXTENSIONS = {'.png', '.jpg', '.jpeg', '.gif'}

class FileUpload:
    def __init__(
        self,
        max_size: int = 1024 ** 5
    ):
        self.max_size = max_size
        self.allow_extensions = ALLOWED_EXTENSIONS
        self.uploads_dir = settings.UploadPath
        self.service='s3' 
        self.aws_access_key_id= settings.aws_access_key_id
        self.aws_secret_access_key= settings.aws_secret_access_key

 
    async def save_file(self, filename):
        file_path = os.path.join(self.uploads_dir, filename)
        session = get_session()
        
        buffer = io.BytesIO()                
        async with aiofiles.open(file_path, "rb"):
            async with session.create_client(self.service, aws_access_key_id=self.aws_access_key_id,
                aws_secret_access_key=self.aws_secret_access_key) as client:
                file_upload_response = await client.put_object(ACL="public-read", Bucket='networkedstorage', Key=filename, Body=buffer.getvalue())
                if file_upload_response["ResponseMetadata"]["HTTPStatusCode"] == 200:
                    logger.info(f"https://networkedstorage.s3.eu-west-2.amazonaws.com/{filename}")
                    return {'url': f'https://networkedstorage.s3.eu-west-2.amazonaws.com/{filename}', 'filename':filename}
                else:
                    raise HTTPException(status_code=400, detail="Failed to upload in S3")

    async def compress(self, source_file, destination_filename: str ):
        img = Image.open(source_file)
        if img.mode in ("RGBA", "P"):
            img_conv = img.convert('RGB')
            img_conv.save(os.path.join(self.uploads_dir, destination_filename), optimize=True, quality=65)
            img_conv.close()
        else:
            img.save(os.path.join(self.uploads_dir, destination_filename), optimize=True, quality=65)
            img.close()
        return await self.save_file(destination_filename)  

   
            
    async def pass_file(self, file: UploadFile):
        filename = file.filename
        content = await file.read()
        file_size = len(content)
        if file_size > self.max_size:
            raise FileMaxSizeLimit(f"File size {file_size} exceeds max size {self.max_size}")
        if self.allow_extensions:
            source_extension = os.path.splitext(filename)[1]
            if not source_extension in self.allow_extensions:
                raise FileExtNotAllowed(
                        f"File ext {source_extension} is not allowed of {self.allow_extensions}"
                    )
            print(settings.UploadPath)
            destination_filename = uuid4().hex + source_extension            
        return await self.compress(destination_filename= destination_filename, source_file=file.file)








