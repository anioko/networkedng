
import datetime, pyotp, json
from time import time
from sqlalchemy import Column, PrimaryKeyConstraint, SmallInteger,\
    desc, String, Integer, ForeignKey, Boolean, DateTime, Text, BigInteger, func, SmallInteger, Float
from sqlalchemy.orm import relationship, backref
from db.base import Base
from models.profiles import *
from sqlalchemy import or_, and_
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import backref
from sqlalchemy_mptt.mixins import BaseNestedSets
from config.config import settings, Templates
from itsdangerous import BadSignature, SignatureExpired
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from utils.dep import pretty_date, redis_q, db
from utils.email import send_email

def get_level(points):
    if points < 1:
        return None
    elif points < 10 and points >= 1:
        return 'Rookie'
    elif points >= 10 and points < 50:
        return 'Newbie'
    elif points >= 50 and points < 150:
        return 'Apprentice'
    elif points >= 150 and points < 500:
        return 'Guru'
    elif points >= 500 and points < 1000:
        return 'Sage'
    elif points >= 1000 and points < 5000:
        return 'Maestro'
    else:
        return 'Rock Star'


class Permission:
    GENERAL = 'GENERAL'
    ADMINISTER = 'ADMINISTRATOR'
    MARKETERS = 'PROMOTER'
    EDITOR = 'EDITOR'
    CUSTOMERCARE = 'CUSTOMERCARE'


class Role(Base):
    __tablename__ = 'roles'
    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    name = Column(String(64), unique=True)
    index = Column(String(64))
    default = Column(Boolean, default=False, index=True)
    permissions = Column(Integer)
    users = relationship('User', backref='role', lazy='dynamic')
    access_role = Column(String(70), unique=True)

    @staticmethod
    def insert_roles():
        roles = {
            'User': (Permission.GENERAL, 'main', True),
            'Promoter': (Permission.MARKETERS, 'marketer',False ),
            'Editor': (Permission.EDITOR, 'editor',False ),
            'CustomerCare': (Permission.CUSTOMERCARE, 'customercare',False ),
            'Administrator': (
                Permission.ADMINISTER,
                'admin',
                False  # grants all permissions
            )
        }
        for r in roles:
            role = db.session.query(Role).filter_by(name=r).first()
            if role is None:
                role = Role(name=r)
            role.access_role = roles[r][0]
            role.index = roles[r][1]
            role.default = roles[r][2]
            db.session.add(role)
        db.session.commit()

    def __repr__(self):
        return '<Role \'%s\'>' % self.name


class Follower(Base):
    __tablename__ = 'followers'
    id = Column(Integer, autoincrement=True, primary_key=True, index=True)
    follower_id = Column(Integer, ForeignKey('users.id', ondelete='CASCADE'))
    followed_id = Column(Integer, ForeignKey('users.id', ondelete='CASCADE'))
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now())


# @whooshee.register_model('first_name', 'last_name', 'city', 'state', 'country', 'profession')
class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, autoincrement=True, primary_key=True, index=True)
    confirmed = Column(Boolean, default=False)
    verified = Column(Boolean, default=False)
    first_name = Column(String(64), index=True)
    last_name = Column(String(64), index=True)
    email = Column(String(64), unique=True, index=True)
    gender = Column(String(64), index=True)
    profession = Column(String(64), index=True)
    area_code = Column(String(6), index=True)
    mobile_phone = Column(BigInteger, unique=True, index=True)
    summary_text = Column(Text)
    zip = Column(String(10), index=True)
    city = Column(String(64), index=True)
    state = Column(String(64), index=True)
    country = Column(String(64), index=True)
    password_hash = Column(String(128))
    socket_id = Column(String(256))
    online = Column(String(1), default='N')
    invited_by = Column(String(128))
    role_id = Column(Integer, ForeignKey('roles.id', ondelete="CASCADE"))
    interests = relationship('Interest', backref='user',
                             lazy='dynamic')
    posts = relationship('Post', backref='user', lazy='dynamic')
    resume = relationship('Resume', backref='user', lazy='dynamic', cascade='all')

    comments = relationship('Comment', backref='user', lazy='dynamic')
    photos = relationship('Photo', backref='user',
                          lazy='dynamic')

    followed = relationship('User',
                            secondary='followers',
                            primaryjoin=(Follower.follower_id == id),
                            secondaryjoin=(Follower.followed_id == id),
                            backref=backref('followers', lazy='dynamic'),
                            lazy='dynamic')

    messages_received = relationship('Message',
                                     foreign_keys='Message.recipient_id',
                                     backref='recipient', lazy='dynamic')
    professional_messages_received = relationship('ProfileMessage',
                                                  foreign_keys='ProfileMessage.recipient_id',
                                                  backref='recipient', lazy='dynamic')
    last_message_read_time = Column(DateTime)
    notifications = relationship('Notification', backref='user',
                                 lazy='dynamic')
    positions_created = relationship('Job', backref='user', lazy='subquery', cascade='all')
    questions = relationship('Question', backref='user', lazy='dynamic')
    newscaster_badge = Column(String())
    ambassador_badge = Column(String())
    networker_badge = Column(String())
    kw_builder_badge = Column(String())
    kw_seeker_badge = Column(String())
    is_director = Column(Boolean, default=False)
    newscaster_points = Column(Integer, default=0)
    ambassador_points = Column(Integer, default=0)
    networker_points = Column(Integer, default=0)
    kw_builder_points = Column(Integer, default=0)
    kw_seeker_points = Column(Integer, default=0)
    user_applicants = relationship('Application', backref='user', lazy='joined')
    user_submissions = relationship('Submission', backref='user', lazy='joined')
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now())
    otp_secret = Column(String(16))
    otp_created_time = Column(DateTime, default=func.now())

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)
        if self.role is None:
            if self.email == settings.ADMIN_EMAIL:
                self.role = db.session.query(Role).filter_by(
                    access_role=Permission.ADMINISTER).first()
            if self.role is None:
                self.role = db.session.query(Role).filter_by(default=True).first()

    @hybrid_property
    def full_name(self):
        return self.first_name + " " + self.last_name


    def can(self, access):
        return self.role is not None and self.role.access_role == access or self.role.access_role == Permission.ADMINISTER

    def is_admin(self):
        return self.can(Permission.ADMINISTER)

    def is_marketer(self):
        return self.can(Permission.MARKETER)

    @staticmethod
    def generate_fake(count=100, **kwargs):
        from sqlalchemy.exc import IntegrityError
        from random import seed, choice
        from faker import Faker

        fake = Faker()
        roles = db.session.query(Role).all()
        if len(roles) <= 0:
            Role.insert_roles()
            roles = db.session.query(Role).all()

        seed()
        for i in range(count):
            u = User(
                first_name=fake.first_name(),
                last_name=fake.last_name(),
                email=fake.email(),
                profession=fake.job(),
                city=fake.city(),
                zip=fake.postcode(),
                state=fake.state(),
                summary_text=fake.text(),
                password_hash='password',
                confirmed=True,
                role=choice(roles),
                **kwargs)
            db.session.add(u)
            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()

    def generate_email_confirmation_token(self, expiration=604800):
        s = Serializer(settings.SECRET_KEY, expiration)
        return str(s.dumps({'confirm': self.id}).decode())

    def generate_confirmation_token(self, interval=300):
        totp = pyotp.TOTP('base32secret3232')
        _token = totp.now()
        self.otp_secret = _token
        self.otp_created_time = datetime.now()
        db.session.add(self)
        db.session.commit()
        return self.otp_secret

    def generate_email_change_token(self, new_email, expiration=3600):
        s = Serializer(settings.SECRET_KEY, expiration)
        return s.dumps({'change_email': self.id, 'new_email': new_email})

    def generate_password_reset_token(self, expiration=3600):
        s = Serializer(settings.SECRET_KEY, expiration)
        return str(s.dumps({'reset': self.id}).decode())

    def confirm_account(self, token):
        s = Serializer(settings.SECRET_KEY)
        try:
            data = s.loads(token)
        except (BadSignature, SignatureExpired):
            return False
        if data.get('confirm') != self.id:
            return False
        self.confirmed = True
        db.session.add(self)
        db.session.commit()
        return True

    def change_email(self, token):
        s = Serializer(settings.SECRET_KEY)
        try:
            data = s.loads(token)
        except (BadSignature, SignatureExpired):
            return False
        if data.get('change_email') != self.id:
            return False
        new_email = data.get('new_email')
        if new_email is None:
            return False
        if db.session.query(User).filter_by(email=new_email).first() is not None:
            return False
        self.email = new_email
        db.session.add(self)
        db.session.commit()
        return True

    def reset_password(self, token, new_password):
        s = Serializer(settings.SECRET_KEY)
        try:
            data = s.loads(token)
        except (BadSignature, SignatureExpired):
            return False
        if data.get('reset') != self.id:
            return False
        self.password_hash = self.get_password_hash(new_password)
        db.session.add(self)
        db.session.commit()
        return True

    def get_photo(self):
        photos = self.photos.all()
        if len(photos) > 0:
            return photos[0].image_url
        else:
            if self.gender == 'Female':
                return "https://1.semantic-ui.com/images/avatar/large/veronika.jpg"
            else:
                return "https://1.semantic-ui.com/images/avatar/large/jenny.jpg"


    def unfollow(self, user):
        if self.is_following(user):
            self.followed.remove(user)
            return self

    def is_following(self, user):
        return self.followed.filter(Follower.followed_id == user.id).count() > 0
   

    def follow(self, user):
        if not self.is_following(user):
            self.followed.append(user)
            return self     

    def followed_posts(self):
        followed = db.session.query(Post).join(
            Follower, (Follower.followed_id == Post.user_id)).filter(
            Follower.follower_id == self.id)
        own = db.session.query(Post).filter_by(user_id=self.id)
        return followed.union(own).order_by(Post.timestamp.desc())

    def last_message(self, user_id):
        message = db.session.query(Message).order_by(Message.timestamp.desc()). \
            filter(or_(and_(Message.recipient_id == user_id, Message.user_id == self.id),
                       and_(Message.recipient_id == self.id, Message.user_id == user_id))).first()
        return message

    def last_professional_message(self, user_id, profile_id):
        message = db.session.query(ProfileMessage).filter_by(profile_id=profile_id).order_by(ProfileMessage.timestamp.desc()). \
            filter(or_(and_(ProfileMessage.recipient_id == user_id, ProfileMessage.user_id == self.id),
                       and_(ProfileMessage.recipient_id == self.id, ProfileMessage.user_id == user_id))).first()
        return message

    def history(self, user_id, unread=False):
        messages = db.session.query(Message).order_by(Message.timestamp.asc()). \
            filter(or_(and_(Message.recipient_id == user_id, Message.user_id == self.id),
                       and_(Message.recipient_id == self.id, Message.user_id == user_id))).all()
        return messages

    def professional_message_history(self, user_id, profile_id, unread=False):
        messages = db.session.query(ProfileMessage).filter_by(profile_id=profile_id).order_by(ProfileMessage.timestamp.asc()). \
            filter(or_(and_(ProfileMessage.recipient_id == user_id, ProfileMessage.user_id == self.id),
                       and_(ProfileMessage.recipient_id == self.id, ProfileMessage.user_id == user_id))).all()
        return messages

    def new_messages(self, user_id=None):
        if not user_id:
            return db.session.query(Message).filter_by(recipient=self).filter(Message.read_at == None).distinct('user_id').count()
        else:
            return db.session.query(Message).filter_by(recipient=self).filter(Message.read_at == None).filter(
                Message.user_id == user_id).count()

    def new_professional_messages(self, profile_id, user_id=None):
        if not user_id:
            return db.session.query(ProfileMessage).filter_by(profile_id=profile_id).filter_by(recipient=self).filter(
                ProfileMessage.read_at == None).distinct('user_id').count()
        else:
            return db.session.query(ProfileMessage).filter_by(profile_id=profile_id).filter_by(recipient=self).filter(
                ProfileMessage.read_at == None).filter(
                ProfileMessage.user_id == user_id).count()

    def add_notification(self, name, data, related_id, permanent=False):
        n = Notification(name=name, payload_json=data, user=self, related_id=related_id)
        db.session.add(n)
        db.session.commit()
        n = db.session.query(Notification).get(n.id)
        body = {
            'user': self.full_name,
            'link': 'main.notifications',
            'notification': n,
            'APP_NAME':settings.APP_NAME
        }
        template = Templates.get_template('/account/email/notification.jinja2')
        template_obj = template.render(**body)        
        redis_q.enqueue(
        send_email,
            recipient=self.email,
            subject='You Have a new notification on Networked',
            template=template_obj,
            body=body
            )
        
        return n

    def get_post_karma(self):
        """
        fetch the number of votes this user has had on his/her posts
        1.) Get id's of all posts by this user
        2.) See how many of those posts also were upvoted but not by
        the person him/her self.
        """
        post_ids = [t.id for t in self.posts]
        select = db.session.query(PostUpvote).select(db.and_(
            PostUpvote.post_id.in_(post_ids),
            PostUpvote.user_id != self.id
        )
        )
        rs = db.engine.execute(select)
        return rs.rowcount

    def get_comment_karma(self):
        """
        fetch the number of votes this user has had on his/her comments
        """
        comment_ids = [c.id for c in self.comments]
        select = db.session.query(CommentUpvote).select(db.and_(
            CommentUpvote.comment_id.in_(comment_ids),
            CommentUpvote.user_id != self.id
        )
        )
        rs = db.engine.execute(select)
        return rs.rowcount






    def __repr__(self):
        return '<User \'%s\'>' % self.full_name


class EntryTag(Base):
    __tablename__ = 'entry_tags'
    __table_args__ = (PrimaryKeyConstraint('question_id', 'tag_id'), {})
    tag_id = Column('tag_id', Integer, ForeignKey('tags.id', ondelete='CASCADE'), nullable=False)
    question_id = Column('question_id', Integer, ForeignKey('questions.id', ondelete='CASCADE'),
                         nullable=False)
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())


class Tag(Base):
    __tablename__ = 'tags'
    id = Column(Integer, primary_key=True)
    tag = Column(String(25))
    questions = relationship('Question', secondary='entry_tags', backref='tag')


class PositionApplication(Base):
    __tablename__ = 'job_applications'
    id = Column(Integer, primary_key=True)
    application_id = Column(Integer, ForeignKey('applications.id', ondelete="CASCADE"))
    position_id = Column(Integer, ForeignKey('jobs.id', ondelete="CASCADE"))
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())


class PromoSubmission(Base):
    __tablename__ = 'promo_submissions'
    id = Column(Integer, primary_key=True)
    submission_id = Column(Integer, ForeignKey('submissions.id', ondelete="CASCADE"))
    promo_id = Column(Integer, ForeignKey('promos.id', ondelete="CASCADE"))
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())


class EventAttendee(Base):
    __tablename__ = 'event_attendees'
    id = Column(Integer, primary_key=True)
    attendee_id = Column(Integer, ForeignKey('attendees.id', ondelete="CASCADE"))
    event_id = Column(Integer, ForeignKey('events.id', ondelete="CASCADE"))
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())


class Message(Base):
    __tablename__ = 'messages'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id', ondelete='cascade'))
    recipient_id = Column(Integer, ForeignKey('users.id', ondelete="CASCADE"))
    body = Column(Text)
    timestamp = Column(DateTime, index=True, default=func.now())
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())
    read_at = Column(DateTime, default=None, nullable=True)

    user = relationship('User', primaryjoin="Message.user_id==User.id")

    def __repr__(self):
        return '<Message {}>'.format(self.body)


class Interest(Base):
    __tablename__ = 'interests'
    id = Column(Integer, primary_key=True)
    name = Column(String(), unique=True)
    desc = Column(String())
    creator_id = Column(Integer, ForeignKey('users.id', ondelete="CASCADE"))
    posts = relationship('Post', backref='interest', lazy='dynamic')
    status = Column(SmallInteger)
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())

    def __init__(self, name, desc, admin_id):
        self.name = name
        self.desc = desc
        self.admin_id = admin_id

    def __repr__(self):
        return '<interest %r>' % self.name

    def get_posts(self, order_by='timestamp'):
        if order_by == 'timestamp':
            return self.posts.order_by(desc(Post.created_at)). \
                all()
        else:
            return self.posts.order_by(desc(Post.created_at)). \
                all()

    def get_age(self):
        return (self.created_at - datetime(1970, 1, 1)).total_seconds()

    def pretty_date(self, typeof='created'):
        if typeof == 'created':
            return pretty_date(self.created_at)
        elif typeof == 'updated':
            return pretty_date(self.updated_at)


# @whooshee.register_model('org_name', 'org_description')
class Organisation(Base):
    __tablename__ = 'organisations'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id', ondelete="CASCADE"), nullable=False)
    image_filename = Column(String, default=None, nullable=True)
    image_url = Column(String, default=None, nullable=True)
    org_name = Column(String(255))
    org_city = Column(String(255))
    org_state = Column(String(255))
    marketer_id = Column(Integer, nullable=True)
    company_registration_number = Column(String(70), default=None, nullable=True)
    org_country = Column(String(255))
    org_website = Column(String(255))
    org_industry = Column(String(255))
    archived = Column(Boolean, default=False)
    org_description = Column(Text)
    logos = relationship('Logo', backref='organisation', lazy='dynamic')
    user = relationship('User', backref='organisations', cascade='all, delete')
    jobs = relationship('Job', backref='organisation')
    promos = relationship('Promo', backref='organisation')
    services = relationship('Service', backref='organisation')
    products = relationship('Product', backref='organisation')
    events = relationship('Event', backref='organisation')
    positions = relationship('Job', backref='organisation_positions',
                             primaryjoin="Organisation.id==Job.organisation_id")
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())

    def __repr__(self):
        return u'<{self.__class__.__name__}: {self.id}>'.format(self=self)

    def get_staff(self):
        ids = [user.user_id for user in self.staff]
        return User.query.filter(User.id.in_(ids)).all()

    def get_photo(self):
        if self.image_filename:
            return self.image_url
        else:
            return None


class Logo(Base):
    __tablename__ = 'logos'
    id = Column(Integer, primary_key=True)
    image_filename = Column(String, default=None, nullable=True)
    image_url = Column(String, default=None, nullable=True)
    organisation_id = Column(Integer, ForeignKey('organisations.id', ondelete="CASCADE"), nullable=False)
    owner_organisation = Column(String(128))
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())

    def __repr__(self):
        return u'<{self.__class__.__name__}: {self.id}>'.format(self=self)


# Uncomment to use
class Product_Image(Base):
    __tablename__ = 'product_images'
    id = Column(Integer, primary_key=True)
    image_filename = Column(String, default=None, nullable=True)
    image_url = Column(String, default=None, nullable=True)
    product_id = Column(Integer, ForeignKey('products.id', ondelete="CASCADE"), nullable=False)
    owner_product = Column(String(128))
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())

    def __repr__(self):
        return u'<{self.__class__.__name__}: {self.id}>'.format(self=self)


class Product(Base):
    __tablename__ = 'products'
    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    org_id = Column(Integer, ForeignKey('organisations.id', ondelete="CASCADE"), nullable=False)
    image_filename = Column(String, default=None, nullable=True)
    image_url = Column(String, default=None, nullable=True)
    product_name = Column(String(255))
    product_description = Column(Text)
    product_price = Column(Integer)
    price_currency = Column(String(255))
    min_order_quantity = Column(Integer)
    product_availability = Column(String(255))
    product_category = Column(String(255))
    product_images = relationship('Product_Image', backref='product', lazy='dynamic')
    product_length = Column(Integer)
    product_weight = Column(Integer)
    product_height = Column(Integer)
    delivery_terms = Column(String(255))
    lead_time = Column(String(255))
    # organisation = relationship('Organisation', backref='products', cascade='all, delete')
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())

    def __repr__(self):
        return u'<{self.__class__.__name__}: {self.id}>'.format(self=self)

    def get_photo(self):
        if self.image_filename:
            return self.image_url
        else:
            return None


class Photo(Base):
    __tablename__ = 'photos'
    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    image_filename = Column(String, default=None, nullable=True)
    image_url = Column(String, default=None, nullable=True)
    user_id = Column(Integer(), ForeignKey(User.id, ondelete="CASCADE"))
    question_id = Column(Integer, ForeignKey('questions.id', ondelete="CASCADE"))
    answer_id = Column(Integer, ForeignKey('answers.id', ondelete="CASCADE"))
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())

    def __repr__(self):
        return u'<{self.__class__.__name__}: {self.id}>'.format(self=self)


class Question(Base):
    __tablename__ = 'questions'
    id = Column(Integer, primary_key=True)
    title = Column(String)
    description = Column(String)
    timestamp = Column(DateTime, index=True, default=func.now())
    user_id = Column(Integer, ForeignKey('users.id', ondelete="CASCADE"))
    author = Column(String(128))
    tags = relationship('Tag', secondary='entry_tags', backref='question')
    answers = relationship('Answer', backref='question', lazy='dynamic')
    photos = relationship('Photo', backref='question_photos', lazy='dynamic')
    comments = relationship('Comment', backref='question_comments', lazy='dynamic')
    level = Column(Integer)
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())

    """def profile(self):
        profile = Profile.query.filter(user_id=current_user.id).first()
        return profile.id"""


class Answer(Base, BaseNestedSets):
    __tablename__ = 'answers'
    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    body = Column(String, index=True)
    timestamp = Column(DateTime, index=True, default=func.now())
    user_id = Column(Integer, ForeignKey('users.id', ondelete="CASCADE"))
    author = Column(String(128))
    question_id = Column(Integer, ForeignKey('questions.id', ondelete="CASCADE"))
    photos = relationship('Photo', backref='answers_photos', lazy='dynamic')
    creator = relationship('User')
    image_url = Column(String, default=None, nullable=True)
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())


class Notification(Base):
    __tablename__ = 'notifications'
    id = Column(Integer, primary_key=True)
    name = Column(String(128), index=True)
    user_id = Column(Integer, ForeignKey('users.id', ondelete="CASCADE"))
    related_id = Column(Integer, default=0)
    timestamp = Column(Float, index=True, default=time)
    payload_json = Column(Text)
    read = Column(Boolean, default=False)
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())

    def __repr__(self):
        return '<Notification {}>'.format(self.name)


           
    def parsed(self):
        user = db.session.query(User).filter_by(id=self.related_id).first()
        if 'unread_message' in self.name:
            msg = db.session.query(Message).query.filter_by(id=self.payload_json).first()
            if user and msg:
                return {
                    "id":self.id,
                    "type": self.name,
                    "text": " {} sent you a message {} ...".format(
                        user.full_name, msg.body[:40].replace("\n", " ")),
                    "timestamp": pretty_date(self.created_at),
                    "time": self.timestamp,
                    #"user": user,
                    "read": self.read
                }
            else:
                self.read = True
                db.session.add(self)
                db.session.commit()
        elif 'unread_professional_message' in self.name:

            msg = db.session.query(ProfileMessage).filter_by(id=self.payload_json).first()
            if user and msg:
                return {
                    "id":self.id,
                    "type": self.name,
                    "text": "{} sent you a new professional message {} ...".format(
                        user.full_name, msg.body[:40].replace("\n", " ")),
                    "timestamp": pretty_date(self.created_at),
                    "time": self.timestamp,
                    #"user": user,
                    "read": self.read
                }
            else:
                self.read = True
                db.session.add(self)
                db.session.commit()

        elif 'post_likes' in self.name:
            #post = Post.query.filter_by(id=self.payload_json).first()
            if user:
                return {
                    "id":self.id,
                    "type": self.name,
                    "title": "New Post Likes",
                    "text": "{} likes your post  ...".format(
                        user.full_name), #, post.text[:40].replace("\n", " ")),
                    "timestamp": pretty_date(self.created_at),
                    "time": self.timestamp,
                    #"user": user,
                    "read": self.read
                }
            else:
                self.read = True
                db.session.add(self)
                db.session.commit()
        elif 'post_replies' in self.name:
            post = db.session.query(Post).filter_by(id=self.related_id)
            comment = db.session.query(Comment).filter_by(text=self.payload_json).first()
            if user:
                return {
                    "id":self.id,
                    "type": self.name,
                    "title": "New Post Replies",
                    "text": "{} commented on your post".format(
                        user.full_name),
                    "timestamp": pretty_date(self.created_at),
                    "time": self.timestamp,
                    #"user": user,
                    "read": self.read
                }
            else:
                self.read = True
                db.session.add(self)
                db.session.commit()
        elif 'answer' in self.name:
            question = db.session.query(Question).filter_by(id=self.related_id).first()
            if question:
                return {
                    "id":self.id,
                    "type": self.name,
                    "title": "New Question Answers",
                    "text": "New answer to your question {} ...".format(question.title[:40]),
                    "timestamp": pretty_date(self.created_at),
                    "time": self.timestamp,
                    #"user": user,
                    "read": self.read
                }
            else:
                self.read = True
                db.session.add(self)
                db.session.commit()
        elif 'new_follower' in self.name:
                if user:
                    return {
                    "id":self.id,    
                    "type": self.name,
                    "title": "New Followers",
                    "text": "{} followed you, click here to go to his profile.".format(
                        user.full_name),
                    "timestamp": pretty_date(self.created_at),
                    "time": self.timestamp,
                    #"user": user,
                    "read": self.read

                }
                else:
                    self.read = True
                    db.session.add(self)
                    db.session.commit()
        elif 'new_post_of_followers' in self.name:
            post = db.session.query(Post).filter_by(id=json.loads(self.payload_json)['post']).first()
            if user and post:
                return {
                    "id":self.id,
                    "type": self.name,
                    "title": "New Post",
                    "text": "{} Added a new post {} ...".format(
                        user.full_name, post.text[:20]),
                    "timestamp": pretty_date(self.created_at),
                    "time": self.timestamp,
                    #"user": user,
                    "read": self.read
                }
            else:
                self.read = True
                db.session.add(self)
                db.session.commit()

        elif 'new_job' in self.name:
            job = db.session.query(Job).filter_by(id=json.loads(self.payload_json)['job']).first()
            if user and job:
                return {
                    "id":self.id,
                    "type": self.name,
                    "title": "New Job vacancy",
                    "text": "{} Added a new job vacancy near you {} ...".format(
                        user.full_name, job.position_title[:20]),
                    "timestamp": pretty_date(self.created_at),
                    "time": self.timestamp,
                    #"user": user,
                    "read": self.read
                }
            else:
                self.read = True
                db.session.add(self)
                db.session.commit()
    
    


class Job(Base):
    __tablename__ = 'jobs'
    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    organisation_id = Column(Integer, ForeignKey('organisations.id', ondelete="CASCADE"), nullable=True)
    image_filename = Column(String, default=None, nullable=True)
    pub_date = Column(String, default=func.now(), nullable=False)
    end_date = Column(String, nullable=False)
    position_title = Column(String(255))
    position_city = Column(String(255))
    position_state = Column(String(255))
    position_country = Column(String(255))
    required_skill_one = Column(String(255))
    required_skill_two = Column(String(255))
    required_skill_three = Column(String(255))
    required_skill_four = Column(String(255))
    required_skill_five = Column(String(255))
    required_skill_six = Column(String(255))
    required_skill_seven = Column(String(255))
    required_skill_eight = Column(String(255))
    required_skill_nine = Column(String(255))
    required_skill_ten = Column(String(255))
    description = Column(Text)
    creator_id = Column(Integer, ForeignKey('users.id', ondelete="CASCADE"), nullable=False)
    applications = relationship("Application", secondary='job_applications',
                                backref=backref("positions", cascade='all'),
                                primaryjoin='Job.id==Application.position_id', cascade='all,delete')
    creator = relationship("User")
    created_at = Column(String, default=func.now())
    updated_at = Column(String, default=func.now(), onupdate=func.now())

    @property
    def org_name(self):
        return Organisation.get(self.organisation_id).org_name

    def __repr__(self):
        return u'<{self.__class__.__name__}: {self.id}>'.format(self=self)


class Promo(Base):
    __tablename__ = 'promos'
    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    organisation_id = Column(Integer, ForeignKey('organisations.id', ondelete="CASCADE"), nullable=True)
    image_filename = Column(String, default=None, nullable=True)
    pub_date = Column(String, nullable=False, default=func.now())
    end_date = Column(String, nullable=False)
    promo_title = Column(String(255))
    promo_city = Column(String(255))
    promo_state = Column(String(255))
    promo_country = Column(String(255))
    requirement_one = Column(String(255))
    requirement_two = Column(String(255))
    requirement_three = Column(String(255))
    requirement_four = Column(String(255))
    requirement_five = Column(String(255))
    requirement_six = Column(String(255))
    requirement_seven = Column(String(255))
    requirement_eight = Column(String(255))
    requirement_nine = Column(String(255))
    requirement_ten = Column(String(255))
    description = Column(Text)
    creator_id = Column(Integer, ForeignKey('users.id', ondelete="CASCADE"), nullable=False)
    submissions = relationship("Submission", secondary='promo_submissions',
                               backref=backref("promos", cascade='all'),
                               primaryjoin='Promo.id==Submission.promo_id', cascade='all,delete')
    creator = relationship("User")
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())

    @property
    def org_name(self):
        return Organisation.get(self.organisation_id).org_name

    def __repr__(self):
        return u'<{self.__class__.__name__}: {self.id}>'.format(self=self)


#### Uncomment to put to use ###
# class product(Base):
##    __tablename__ = 'products'
##    id = Column(Integer, primary_key=True)
##    organisation_id = Column(Integer, ForeignKey('organisations.id', ondelete="CASCADE"), nullable=True)
##    image_filename = Column(String, default=None, nullable=True)
##    image_url = Column(String, default=None, nullable=True)
##    pub_date = Column(DateTime, default=datetime.now, nullable=False)
##    product_category = Column(String(255))
##    product_name = Column(String(255))
##    brand_name = Column(String(255))
##    product_type = Column(String(255))
##    product_size = Column(String(255))
##    material = Column(String(255))
##    min_order_quantity = Column(Integer)
##    lead_time = Column(String(255))
##    product_weight = Column(String(255))
##    delivery_terms = Column(String(255))
##    description = Column(Text)
##    creator_id = Column(Integer, ForeignKey('users.id', ondelete="CASCADE"), nullable=False)
##    creator = relationship("User")
##    created_at = Column(DateTime, default=datetime.now)
##    updated_at = Column(DateTime, default=datetime.now, onupdate=datetime.now)
##
# @property
# def org_name(self):
# return Organisation.get(self.organisation_id).org_name
##
# def get_photo(self):
# if self.image_filename:
# return url_for('_uploads.uploaded_file', setname='images', filename=self.image_filename, _external=True)
# else:
# return self.organisation.get_photo()
##
# def __repr__(self):
# return u'<{self.__class__.__name__}: {self.id}>'.format(self=self)


class Service(Base):
    __tablename__ = 'service'
    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    organisation_id = Column(Integer, ForeignKey('organisations.id', ondelete="CASCADE"), nullable=True)
    pub_date = Column(DateTime, default=func.now(), nullable=False)
    service_category = Column(String(255))
    service_title = Column(String(255))
    service_city = Column(String(255))
    service_state = Column(String(255))
    service_country = Column(String(255))
    mobile_phone = Column(BigInteger, index=True)
    street_address = Column(String(255))
    description = Column(Text)
    creator_id = Column(Integer, ForeignKey('users.id', ondelete="CASCADE"), nullable=False)
    creator = relationship("User")
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())

    @property
    def org_name(self):
        return Organisation.get(self.organisation_id).org_name

    def __repr__(self):
        return u'<{self.__class__.__name__}: {self.id}>'.format(self=self)


#@whooshee.register_model('event_title', 'event_state', 'event_country')
class Event(Base):
    __tablename__ = 'events'
    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    organisation_id = Column(Integer, ForeignKey('organisations.id', ondelete="CASCADE"), nullable=True)
    image_filename = Column(String, default=None, nullable=True)
    pub_date = Column(DateTime, default=func.now(), nullable=False)
    start_date = Column(String, default='now', nullable=False)
    end_date = Column(String, default='now', nullable=False)
    event_title = Column(String(255))
    event_city = Column(String(255))
    event_state = Column(String(255))
    event_country = Column(String(255))
    description = Column(Text)
    creator_id = Column(Integer, ForeignKey('users.id', ondelete="CASCADE"), nullable=False)
    attendees = relationship("Attendee", secondary='event_attendees',
                             backref=backref("events", cascade='all'),
                             primaryjoin='Event.id==Attendee.event_id', cascade='all,delete')
    creator = relationship("User")
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())

    @property
    def org_name(self):
        return Organisation.get(self.organisation_id).org_name

    """def get_photo(self):
        if self.image_filename:
            return url_for('_uploads.uploaded_file', setname='images', filename=self.image_filename, _external=True)
        else:
            return self.organisation.get_photo()"""

    def __repr__(self):
        return u'<{self.__class__.__name__}: {self.id}>'.format(self=self)


class Attendee(Base):
    __tablename__ = 'attendees'
    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    event_id = Column(Integer, ForeignKey('events.id', ondelete="CASCADE"), nullable=False)
    user_id = Column(Integer, ForeignKey('users.id', ondelete="CASCADE"), nullable=False)
    event = relationship('Event', cascade='all, delete')
    users = relationship('User', order_by=User.id, backref="attendees", cascade="all")
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())


class Submission(Base):
    __tablename__ = 'submissions'
    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    promo_id = Column(Integer, ForeignKey('promos.id', ondelete="CASCADE"), nullable=False)
    user_id = Column(Integer, ForeignKey('users.id', ondelete="CASCADE"), nullable=False)
    event = relationship('Promo', cascade='all, delete')
    users = relationship('User', order_by=User.id, backref="submissions", cascade="all")
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())


class ApplicationExtra(Base):
    __tablename__ = 'application_extras'
    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    application_id = Column(Integer, ForeignKey('applications.id', ondelete="CASCADE"))
    extra_id = Column(Integer, ForeignKey('extras.id', ondelete="CASCADE"))
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())


class Application(Base):
    __tablename__ = 'applications'
    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    extras = relationship('Extra', secondary='application_extras', backref='applications')
    position_id = Column(Integer, ForeignKey('jobs.id', ondelete="CASCADE"), nullable=False)
    user_id = Column(Integer, ForeignKey('users.id', ondelete="CASCADE"), nullable=False)
    position = relationship('Job', cascade='all, delete')
    users = relationship('User', order_by=User.id, backref="application", cascade="all")
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())


class Extra(Base):
    __tablename__ = 'extras'
    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    image_filename = Column(String, default=None, nullable=True)
    image_url = Column(String, default=None, nullable=True)
    required_skill_one = Column(String(255))
    required_skill_two = Column(String(255))
    required_skill_three = Column(String(255))
    required_skill_four = Column(String(255))
    required_skill_five = Column(String(255))
    required_skill_six = Column(String(255))
    required_skill_seven = Column(String(255))
    required_skill_eight = Column(String(255))
    required_skill_nine = Column(String(255))
    required_skill_ten = Column(String(255))
    user_id = Column(Integer(), ForeignKey(User.id, ondelete="cascade"))
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())


class PostUpvote(Base):
    __tablename__ = 'post_upvotes'
    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    user_id = Column(Integer, ForeignKey('users.id', ondelete="CASCADE"))
    post_id = Column(Integer, ForeignKey('posts.id', ondelete="CASCADE"))
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())


class CommentUpvote(Base):
    __tablename__ = 'comment_upvotes'
    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    user_id = Column(Integer, ForeignKey('users.id', ondelete="CASCADE"))
    comment_id = Column(Integer, ForeignKey('post_comments.id', ondelete="CASCADE"))
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())


class PostLike(Base):
    __tablename__ = 'post_likes'
    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    user_id = Column(Integer, default=1)
    post_id = Column(Integer, ForeignKey('posts.id', ondelete="CASCADE"))
    user = relationship('User', foreign_keys=[user_id], primaryjoin="User.id==PostLike.user_id")
    post = relationship('Post', foreign_keys=[post_id])
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())


class Post(Base):
    __tablename__ = 'posts'
    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    title = Column(String())
    text = Column(String(), default=None)
    thumbnail = Column(String(), default=None)
    post_privacy = Column(Integer, default=0)
    author = Column(String(128))
    image_filename = Column(Text, default=None, nullable=True)
    image_url = Column(Text, default=None, nullable=True)
    user_id = Column(Integer, ForeignKey('users.id', ondelete="CASCADE"))
    interest_id = Column(Integer, ForeignKey('interests.id', ondelete="CASCADE"))
    comments = relationship('Comment', backref=backref('post'), lazy='dynamic',
                            cascade="all, delete-orphan")
    likes = relationship('PostLike', lazy='dynamic', cascade="all, delete-orphan")
    votes = Column(Integer, default=1)
    hotness = Column(Float(15, 6), default=0.00)
    creator = relationship('User')
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())

    def user_likes(self, user_id):
        likes = PostLike.query.filter_by(post_id=self.id).all()
        user_ids = [like.user_id for like in likes]
        if user_id in user_ids:
            return True
        return False

    def __repr__(self):
        return '<post %r>' % self.title

    def get_comments(self, order_by='timestamp'):
        """
        default order by timestamp
        return only top levels!
        """
        if order_by == 'timestamp':
            return self.comments.filter_by(depth=1). \
                order_by(desc(Comment.created_at)).all()
        else:
            return self.comments.filter_by(depth=1). \
                order_by(desc(Comment.created_at)).all()

    def get_age(self):
        """
        returns the raw age of this post in seconds
        """
        return (self.created_at - datetime(1970, 1, 1)).total_seconds()

    #def get_hotness(self):
        """
        returns the reddit hotness algorithm (votes/(age^1.5))
        """
        """order = log(max(abs(self.votes), 1), 10)  # Max/abs are not needed in our case
        seconds = self.get_age() - 1134028003
        return round(order + seconds / 45000, 6)"""

    def set_hotness(self):
        """
        returns the reddit hotness algorithm (votes/(age^1.5))
        """
        self.hotness = self.get_hotness()
        db.session.commit()

    def pretty_date(self, typeof='created'):
        """
        returns a humanized version of the raw age of this post,
        eg: 34 minutes ago versus 2040 seconds ago.
        """
        if typeof == 'created':
            return pretty_date(self.created_at)
        elif typeof == 'updated':
            return pretty_date(self.updated_at)

    def add_comment(self, comment_text, comment_parent_id, user_id):
        """
        add a comment to this particular post
        """
        if int(comment_parent_id) != 0:
            # parent_comment = Comment.query.get_or_404(comment_parent_id)
            # if parent_comment.depth + 1 > post.MAX_COMMENT_DEPTH:
            #    flash('You have exceeded the maximum comment depth')
            comment_parent_id = int(comment_parent_id)
            comment = Comment(post_id=self.id, user_id=user_id,
                              text=comment_text, parent_id=comment_parent_id)
        else:
            comment = Comment(post_id=self.id, user_id=user_id,
                              text=comment_text)

        db.session.add(comment)
        db.session.commit()
        comment.set_depth()
        return comment


class Comment(Base, BaseNestedSets):
    __tablename__ = 'post_comments'
    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    text = Column(String(), default=None)
    user_id = Column(Integer, ForeignKey('users.id', ondelete="CASCADE"))
    post_id = Column(Integer, ForeignKey('posts.id', ondelete="CASCADE"))
    author = relationship('User')
    depth = Column(Integer, default=1)
    question_id = Column(Integer, ForeignKey('questions.id', ondelete="CASCADE"))
    votes = Column(Integer, default=1)
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())

    def __repr__(self):
        return '<Comment %r>' % (self.text[:25])

    def __init__(self, post_id, user_id, text, parent_id=None):
        self.post_id = post_id
        self.user_id = user_id
        self.text = text
        self.parent_id = parent_id

    def set_depth(self):
        """
        call after initializing
        """
        if self.parent:
            self.depth = self.parent.depth + 1
            db.session.commit()

    def get_comments(self, order_by='timestamp'):
        """
        default order by timestamp
        """
        if order_by == 'timestamp':
            return self.children.order_by(desc(Comment.created_at)). \
                all()
        else:
            return self.comments.order_by(desc(Comment.created_at)). \
                all()

    def get_margin_left(self):
        """
        nested comments are pushed right on a page
        -15px is our default margin for top level comments
        """
        margin_left = 15 + ((self.depth - 1) * 32)
        margin_left = min(margin_left, 680)
        return str(margin_left) + "px"

    def get_age(self):
        """
        returns the raw age of this post in seconds
        """
        return (self.created_at - datetime.datetime(1970, 1, 1)).total_seconds()

    def pretty_date(self, typeof='created'):
        """
        returns a humanized version of the raw age of this post,
        eg: 34 minutes ago versus 2040 seconds ago.
        """
        if typeof == 'created':
            return pretty_date(self.created_at)
        elif typeof == 'updated':
            return pretty_date(self.updated_at)


class OrgStaff(Base):
    __tablename__ = 'org_staff'
    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    user_id = Column(Integer, ForeignKey('users.id', ondelete="CASCADE"))
    invited_by = Column(Integer, ForeignKey('users.id', ondelete="CASCADE"))
    org_id = Column(Integer, ForeignKey('organisations.id', ondelete="CASCADE"))
    user = relationship("User", primaryjoin="User.id==OrgStaff.user_id")
    referer = relationship("User", primaryjoin="User.id==OrgStaff.invited_by")
    org = relationship("Organisation", primaryjoin="Organisation.id==OrgStaff.org_id", backref='staff')
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())


class ContactMessage(Base):
    __tablename__ = 'contact_messages'
    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    user_id = Column(Integer, ForeignKey('users.id', ondelete="CASCADE"), nullable=True)
    name = Column(String(), default=None, nullable=True)
    email = Column(String(64), default=None, nullable=True)
    text = Column(Text)
    user = relationship("User")

    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())


class Resume(Base):
    __tablename__ = 'resumes'
    id = Column(Integer, primary_key=True, autoincrement=True, index=True)

    company_name_one = Column(String(255))
    company_summary_one = Column(String(255))
    role_one = Column(String(255))
    role_description_one = Column(Text)
    start_date_one = Column(DateTime)
    end_date_one = Column(DateTime)
    currently_one = Column(String(3))
    location_city_one = Column(String(255))
    location_state_one = Column(String(255))
    location_country_one = Column(String(255))

    company_name_two = Column(String(255))
    company_summary_two = Column(String(255))
    role_two = Column(String(255))
    role_description_two = Column(Text)
    start_date_two = Column(DateTime)
    end_date_two = Column(DateTime)
    currently_two = Column(String(3))
    location_city_two = Column(String(255))
    location_state_two = Column(String(255))
    location_country_two = Column(String(255))

    company_name_three = Column(String(255))
    company_summary_three = Column(String(255))
    role_three = Column(String(255))
    role_description_three = Column(Text)
    start_date_three = Column(DateTime)
    end_date_three = Column(DateTime)
    currently_three = Column(String(3))
    location_city_three = Column(String(255))
    location_state_three = Column(String(255))
    location_country_three = Column(String(255))

    company_name_four = Column(String(255))
    company_summary_four = Column(String(255))
    role_four = Column(String(255))
    role_description_four = Column(Text)
    start_date_four = Column(DateTime)
    end_date_four = Column(DateTime)
    currently_four = Column(String(3))
    location_city_four = Column(String(255))
    location_state_four = Column(String(255))
    location_country_four = Column(String(255))

    company_name_five = Column(String(255))
    company_summary_five = Column(String(255))
    role_five = Column(String(255))
    role_description_five = Column(Text)
    start_date_five = Column(DateTime)
    end_date_five = Column(DateTime)
    currently_five = Column(String(3))
    location_city_five = Column(String(255))
    location_state_five = Column(String(255))
    location_country_five = Column(String(255))

    company_name_six = Column(String(255))
    company_summary_six = Column(String(255))
    role_six = Column(String(255))
    role_description_six = Column(Text)
    start_date_six = Column(DateTime)
    end_date_six = Column(DateTime)
    currently_six = Column(String(3))
    location_city_six = Column(String(255))
    location_state_six = Column(String(255))
    location_country_six = Column(String(255))

    school_name_one = Column(String(255))
    degree_description_one = Column(String(255))
    grading_one = Column(String(255))
    school_start_date_one = Column(DateTime)
    school_end_date_one = Column(DateTime)
    school_currently_one = Column(String(3))
    state_school_one = Column(String(255))
    city_school_one = Column(String(255))
    country_school_one = Column(String(255))

    school_name_two = Column(String(255))
    degree_description_two = Column(String(255))
    grading_two = Column(String(255))
    school_start_date_two = Column(DateTime)
    school_end_date_two = Column(DateTime)
    school_currently_two = Column(String(3))
    state_school_two = Column(String(255))
    city_school_two = Column(String(255))
    country_school_two = Column(String(255))

    school_name_three = Column(String(255))
    degree_description_three = Column(String(255))
    grading_three = Column(String(255))
    school_start_date_three = Column(DateTime)
    school_end_date_three = Column(DateTime)
    school_currently_three = Column(String(255))
    state_school_three = Column(String(255))
    city_school_three = Column(String(255))
    country_school_three = Column(String(255))

    school_name_four = Column(String(255))
    degree_description_four = Column(String(255))
    grading_four = Column(String(255))
    school_start_date_four = Column(DateTime)
    school_end_date_four = Column(DateTime)
    school_currently_four = Column(String(255))
    state_school_four = Column(String(255))
    city_school_four = Column(String(255))
    country_school_four = Column(String(255))

    school_name_five = Column(String(255))
    degree_description_five = Column(String(255))
    grading_five = Column(String(255))
    school_start_date_five = Column(DateTime)
    school_end_date_five = Column(DateTime)
    school_currently_five = Column(String(255))
    state_school_five = Column(String(255))
    city_school_five = Column(String(255))
    country_school_five = Column(String(255))
    user_id = Column(Integer, ForeignKey('users.id', ondelete="CASCADE"), nullable=False)
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())

    def __repr__(self):
        return u'<{self.__class__.__name__}: {self.id}>'.format(self=self)
