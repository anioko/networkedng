import os, sys

sys.path.append(os.path.dirname(os.path.realpath(__file__)))
import redis
from rq import Worker, Queue, Connection

basedir = os.path.abspath(os.path.dirname(__file__))
env_file = os.path.join(basedir, 'config.env')
if os.path.exists(env_file):
    for line in open(env_file):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1].replace("\"", "")

listen = ['default']

redis_url = os.getenv('REDISTOGO_URL', 'redis://localhost:6379')

conn = redis.from_url(redis_url)

if __name__ == '__main__':
    with Connection(conn):
        worker = Worker(list(map(Queue, listen)))
        worker.work()
