import json
from datetime import datetime
import asyncio
import werkzeug
from flask_login import login_required, current_user
import flask.scaffold

flask.helpers._endpoint_from_view_func = flask.scaffold._endpoint_from_view_func
import flask_restful
from flask_restful import Resource, reqparse
from flask_uploads import UploadSet, IMAGES
from sqlalchemy import or_, and_

from app.models import User, Message, Profile, ProfileSkill, ProfileEdu, ProfileJob, ProfileProject, ProfileLang, \
    ProfileMessage
from app.utils import db, get_paginated_list, jsonify_object, strip_tags, get_lang_name

images = UploadSet('images', IMAGES)


class PostMessage(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('message', help='This field cannot be blank', required=True)

    @login_required
    def post(self, recipient_id):
        data = self.parser.parse_args()
        user = User.query.filter_by(id=recipient_id).first_or_404()
        msg = Message(user_id=current_user.id, recipient_id=recipient_id,
                      body=strip_tags(data['message']))
        db.session.add(msg)
        db.session.commit()
        db.session.refresh(msg)
        user.add_notification('unread_message', {'message': msg.id, 'count': user.new_messages()},
                              permanent=True)
        msg = Message.query.get(msg.id)
        return {
            'status': 1,
            'message': jsonify_object(msg)
        }


class PostProfileMessage(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('message', help='This field cannot be blank', required=True)

    @login_required
    def post(self, recipient_id, profile_id):
        data = self.parser.parse_args()
        user = User.query.filter_by(id=recipient_id).first_or_404()
        profile_instance = Profile.query.filter(or_(Profile.user == user, Profile.user == current_user)).filter_by(
            id=profile_id).first()
        if profile_instance is None:
            return {
                'status': 0,
                'message': "Profile Not Found"
            }
        msg = ProfileMessage(user_id=current_user.id, recipient_id=recipient_id, profile=profile_instance,
                             body=strip_tags(data['message']))
        db.session.add(msg)
        db.session.commit()
        db.session.refresh(msg)
        user.add_notification('unread_professional_message',
                              msg.id,
                              related_id=current_user.id, permanent=True)
        msg = ProfileMessage.query.get(msg.id)
        return {
            'status': 1,
            'message': jsonify_object(msg)
        }


class GetMessages(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('first_page_last', required=False)

    @login_required
    def get(self, user_id, page_id):
        data = self.parser.parse_args()
        user = User.query.filter_by(id=user_id).first_or_404()
        for message in current_user.history(user.id):
            if message.recipient_id == current_user.id:
                message.read_at = db.func.now()
            db.session.add(message)
        db.session.commit()
        messages = Message.query.order_by(Message.timestamp.desc()). \
            filter(or_(and_(Message.recipient_id == user_id, Message.user_id == current_user.id),
                       and_(Message.recipient_id == current_user.id, Message.user_id == user_id)))
        if data['first_page_last']:
            messages = messages.filter(Message.id <= data['first_page_last'])
        messages = messages.paginate(page_id, per_page=20)
        messages = get_paginated_list(messages)
        return {
            'status': 1,
            'messages': messages,
            'now': str(datetime.now())
        }


class GetProfileMessages(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('first_page_last', required=False)

    @login_required
    def get(self, profile_id, user_id, page_id):
        data = self.parser.parse_args()
        user = User.query.filter_by(id=user_id).first_or_404()
        for message in current_user.professional_message_history(user.id, profile_id):
            if message.recipient_id == current_user.id:
                message.read_at = db.func.now()
            db.session.add(message)
        db.session.commit()
        messages = ProfileMessage.query.filter_by(profile_id=profile_id).order_by(ProfileMessage.timestamp.desc()). \
            filter(or_(and_(ProfileMessage.recipient_id == user_id, ProfileMessage.user_id == current_user.id),
                       and_(ProfileMessage.recipient_id == current_user.id, ProfileMessage.user_id == user_id)))
        if data['first_page_last']:
            messages = messages.filter(Message.id <= data['first_page_last'])
        messages = messages.paginate(page_id, per_page=20)
        messages = get_paginated_list(messages)
        return {
            'status': 1,
            'messages': messages,
            'now': str(datetime.now())
        }


class ToggleFollow(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('user_id', help='This field cannot be blank', required=True)

    @login_required
    def post(self):
        data = self.parser.parse_args()
        user_id = data['user_id']
        user = User.query.filter(User.id == user_id).first()
        if user is None:
            return {
                'status': 0,
                'message': "User Not Found"
            }
        if user == current_user:
            return {
                'status': 0,
                'message': "User Not Found"
            }
        if current_user in user.followers:
            current_user.unfollow(user)
            message = "You just un-followed {}".format(user.full_name)
            following = False
            db.session.commit()
        else:
            current_user.follow(user)
            message = "You just followed {}".format(user.full_name)
            following = True
            db.session.commit()
            user.add_notification(name='new_follower', data=message,
                                  related_id=current_user.id,
                                  permanent=True)
        return {
            'status': 1,
            'message': message,
            'following': following
        }


def fill_profile(profile_instance):
    skills = [jsonify_object(skill) for skill in profile_instance.skills]
    education = [jsonify_object(edu, True) for edu in profile_instance.education]
    jobs = [jsonify_object(job, True) for job in profile_instance.jobs]
    languages = [{**jsonify_object(lang), **{'name': get_lang_name(lang.lang)}} for lang in profile_instance.languages]
    projects = [jsonify_object(project, True) for project in profile_instance.projects]
    full_name = profile_instance.full_name
    completeness = profile_instance.completeness
    profile_instance = jsonify_object(profile_instance)
    profile_instance['full_name'] = full_name
    profile_instance['skills'] = skills
    profile_instance['education'] = education
    profile_instance['jobs'] = jobs
    profile_instance['languages'] = languages
    profile_instance['projects'] = projects
    profile_instance['completeness'] = completeness
    return profile_instance


class GetProfileInfo(Resource):
    @login_required
    def get(self, profile_id, first_name, last_name):
        profile_instance = Profile.query.filter_by(user=current_user).filter_by(id=profile_id, first_name=first_name,
                                                                                last_name=last_name).first()
        if profile_instance is None:
            return {
                'status': 0,
                'message': "User Not Found"
            }
        profile = fill_profile(profile_instance)
        return {
            "status": 1,
            "profile": profile
        }


class AddProfileSkill(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('name', help='This field cannot be blank', required=True)
        self.parser.add_argument('description', help='This field cannot be blank', required=True)
        self.parser.add_argument('exp', help='This field cannot be blank', required=True)

    @login_required
    def post(self, profile_id):
        data = self.parser.parse_args()
        profile_instance = Profile.query.filter_by(user=current_user).filter_by(id=profile_id).first()
        if profile_instance is None:
            return {
                'status': 0,
                'message': "Profile Not Found"
            }
        skill = ProfileSkill(name=data['name'], profile=profile_instance,
                             description=data['description'], exp=data['exp'])
        db.session.add(skill)
        db.session.commit()
        db.session.refresh(skill)
        db.session.refresh(profile_instance)
        profile = fill_profile(profile_instance)
        return {
            'status': 1,
            'profile': profile
        }


class DeleteProfileSkill(Resource):
    @login_required
    def post(self, profile_id, skill_id):
        profile_instance = Profile.query.filter_by(user=current_user).filter_by(id=profile_id).first()
        if profile_instance is None:
            return {
                'status': 0,
                'message': "Profile Not Found"
            }
        skill = ProfileSkill.query.filter_by(profile=profile_instance).filter_by(id=skill_id).first()
        if skill is None:
            return {
                'status': 0,
                'message': "Skill Not Found"
            }
        db.session.delete(skill)
        db.session.commit()
        db.session.refresh(profile_instance)
        profile = fill_profile(profile_instance)
        return {
            'status': 1,
            'profile': profile
        }


class EditProfileSkill(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('name', help='This field cannot be blank', required=True)
        self.parser.add_argument('description', help='This field cannot be blank', required=True)
        self.parser.add_argument('exp', help='This field cannot be blank', required=True)

    @login_required
    def post(self, profile_id, skill_id):
        data = self.parser.parse_args()
        profile_instance = Profile.query.filter_by(user=current_user).filter_by(id=profile_id).first()
        if profile_instance is None:
            return {
                'status': 0,
                'message': "Profile Not Found"
            }
        skill = ProfileSkill.query.filter_by(profile=profile_instance).filter_by(id=skill_id).first()
        if skill is None:
            return {
                'status': 0,
                'message': "Skill Not Found"
            }
        skill.name = data['name']
        skill.profile = profile_instance
        skill.description = data['description']
        skill.exp = data['exp']
        db.session.add(skill)
        db.session.commit()
        db.session.refresh(skill)
        db.session.refresh(profile_instance)
        profile = fill_profile(profile_instance)
        return {
            'status': 1,
            'profile': profile
        }


class AddProfileEdu(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('school', help='This field cannot be blank', required=True)
        self.parser.add_argument('degree', help='This field cannot be blank', required=True)
        self.parser.add_argument('start_date', help='This field cannot be blank', required=True)
        self.parser.add_argument('end_date', help='This field cannot be blank', required=False)

    @login_required
    def post(self, profile_id):
        data = self.parser.parse_args()
        profile_instance = Profile.query.filter_by(user=current_user).filter_by(id=profile_id).first()
        if profile_instance is None:
            return {
                'status': 0,
                'message': "Profile Not Found"
            }
        end_date = None if data['end_date'] == '' else data['end_date']
        edu = ProfileEdu(school=data['school'], profile=profile_instance,
                         degree=data['degree'], start_date=data['start_date'], end_date=end_date)
        db.session.add(edu)
        db.session.commit()
        db.session.refresh(edu)
        db.session.refresh(profile_instance)
        profile = fill_profile(profile_instance)
        return {
            'status': 1,
            'profile': profile
        }


class EditProfileEdu(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('school', help='This field cannot be blank', required=True)
        self.parser.add_argument('degree', help='This field cannot be blank', required=True)
        self.parser.add_argument('start_date', help='This field cannot be blank', required=True)
        self.parser.add_argument('end_date', help='This field cannot be blank', required=False)

    @login_required
    def post(self, profile_id, edu_id):
        data = self.parser.parse_args()
        profile_instance = Profile.query.filter_by(user=current_user).filter_by(id=profile_id).first()
        if profile_instance is None:
            return {
                'status': 0,
                'message': "Profile Not Found"
            }
        edu = ProfileEdu.query.filter_by(profile=profile_instance).filter_by(id=edu_id).first()
        if edu is None:
            return {
                'status': 0,
                'message': "Education Not Found"
            }
        end_date = None if data['end_date'] == '' else data['end_date']
        edu.school = data['school']
        edu.profile = profile_instance
        edu.degree = data['degree']
        edu.start_date = data['start_date']
        edu.end_date = end_date
        db.session.add(edu)
        db.session.commit()
        db.session.refresh(edu)
        db.session.refresh(profile_instance)
        profile = fill_profile(profile_instance)
        return {
            'status': 1,
            'profile': profile
        }


class DeleteProfileEdu(Resource):
    @login_required
    def post(self, profile_id, edu_id):
        profile_instance = Profile.query.filter_by(user=current_user).filter_by(id=profile_id).first()
        if profile_instance is None:
            return {
                'status': 0,
                'message': "Profile Not Found"
            }
        edu = ProfileEdu.query.filter_by(profile=profile_instance).filter_by(id=edu_id).first()
        if edu is None:
            return {
                'status': 0,
                'message': "Education Not Found"
            }
        db.session.delete(edu)
        db.session.commit()
        db.session.refresh(profile_instance)
        profile = fill_profile(profile_instance)
        return {
            'status': 1,
            'profile': profile
        }


class AddProfileJob(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('title', help='This field cannot be blank', required=True)
        self.parser.add_argument('company', help='This field cannot be blank', required=True)
        self.parser.add_argument('commitment', help='This field cannot be blank', required=True)
        self.parser.add_argument('start_date', help='This field cannot be blank', required=True)
        self.parser.add_argument('end_date', help='This field cannot be blank', required=False)

    @login_required
    def post(self, profile_id):
        data = self.parser.parse_args()
        profile_instance = Profile.query.filter_by(user=current_user).filter_by(id=profile_id).first()
        if profile_instance is None:
            return {
                'status': 0,
                'message': "Profile Not Found"
            }
        end_date = None if data['end_date'] == '' else data['end_date']
        job = ProfileJob(title=data['title'], profile=profile_instance,
                         company=data['company'], commitment=data['commitment'], start_date=data['start_date'],
                         end_date=end_date)
        db.session.add(job)
        db.session.commit()
        db.session.refresh(job)
        db.session.refresh(profile_instance)
        profile = fill_profile(profile_instance)
        return {
            'status': 1,
            'profile': profile
        }


class EditProfileJob(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('title', help='This field cannot be blank', required=True)
        self.parser.add_argument('company', help='This field cannot be blank', required=True)
        self.parser.add_argument('commitment', help='This field cannot be blank', required=True)
        self.parser.add_argument('start_date', help='This field cannot be blank', required=True)
        self.parser.add_argument('end_date', help='This field cannot be blank', required=False)

    @login_required
    def post(self, profile_id, job_id):
        data = self.parser.parse_args()
        profile_instance = Profile.query.filter_by(user=current_user).filter_by(id=profile_id).first()
        if profile_instance is None:
            return {
                'status': 0,
                'message': "Profile Not Found"
            }
        end_date = None if data['end_date'] == '' else data['end_date']
        job = ProfileJob.query.filter_by(profile=profile_instance).filter_by(id=job_id).first()
        if job is None:
            return {
                'status': 0,
                'message': "Job Not Found"
            }
        job.title = data['title']
        job.profile = profile_instance
        job.company = data['company']
        job.commitment = data['commitment']
        job.start_date = data['start_date']
        job.end_date = end_date
        db.session.add(job)
        db.session.commit()
        db.session.refresh(job)
        db.session.refresh(profile_instance)
        profile = fill_profile(profile_instance)
        return {
            'status': 1,
            'profile': profile
        }


class DeleteProfileJob(Resource):
    @login_required
    def post(self, profile_id, job_id):
        profile_instance = Profile.query.filter_by(user=current_user).filter_by(id=profile_id).first()
        if profile_instance is None:
            return {
                'status': 0,
                'message': "Profile Not Found"
            }
        job = ProfileJob.query.filter_by(profile=profile_instance).filter_by(id=job_id).first()
        if job is None:
            return {
                'status': 0,
                'message': "Job Not Found"
            }
        db.session.delete(job)
        db.session.commit()
        db.session.refresh(profile_instance)
        profile = fill_profile(profile_instance)
        return {
            'status': 1,
            'profile': profile
        }


class AddProfileProject(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('name', help='This field cannot be blank', required=True)
        self.parser.add_argument('description', help='This field cannot be blank', required=True)
        self.parser.add_argument('links', help='This field cannot be blank', action='append', required=True)
        self.parser.add_argument('start_date', help='This field cannot be blank', required=True)
        self.parser.add_argument('end_date', help='This field cannot be blank', required=False)

    @login_required
    def post(self, profile_id):
        data = self.parser.parse_args()
        profile_instance = Profile.query.filter_by(user=current_user).filter_by(id=profile_id).first()
        if profile_instance is None:
            return {
                'status': 0,
                'message': "Profile Not Found"
            }
        end_date = None if data['end_date'] == '' else data['end_date']
        links = json.dumps(data['links'])
        project = ProfileProject(name=data['name'], profile=profile_instance,
                                 description=data['description'], links=links, start_date=data['start_date'],
                                 end_date=end_date)
        db.session.add(project)
        db.session.commit()
        db.session.refresh(project)
        db.session.refresh(profile_instance)
        profile = fill_profile(profile_instance)
        return {
            'status': 1,
            'profile': profile
        }


class EditProfileProject(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('name', help='This field cannot be blank', required=True)
        self.parser.add_argument('description', help='This field cannot be blank', required=True)
        self.parser.add_argument('links', help='This field cannot be blank', action='append', required=True)
        self.parser.add_argument('start_date', help='This field cannot be blank', required=True)
        self.parser.add_argument('end_date', help='This field cannot be blank', required=False)

    @login_required
    def post(self, profile_id, project_id):
        data = self.parser.parse_args()
        profile_instance = Profile.query.filter_by(user=current_user).filter_by(id=profile_id).first()
        if profile_instance is None:
            return {
                'status': 0,
                'message': "Profile Not Found"
            }
        end_date = None if data['end_date'] == '' else data['end_date']
        links = json.dumps(data['links'])
        project = ProfileProject.query.filter_by(profile=profile_instance).filter_by(id=project_id).first()
        if project is None:
            return {
                'status': 0,
                'message': "Project Not Found"
            }
        project.name = data['name']
        project.description = data['description']
        project.links = links
        project.start_date = data['start_date']
        project.end_date = end_date
        db.session.add(project)
        db.session.commit()
        db.session.refresh(project)
        db.session.refresh(profile_instance)
        profile = fill_profile(profile_instance)
        return {
            'status': 1,
            'profile': profile
        }


class DeleteProfileProject(Resource):
    @login_required
    def post(self, profile_id, project_id):
        profile_instance = Profile.query.filter_by(user=current_user).filter_by(id=profile_id).first()
        if profile_instance is None:
            return {
                'status': 0,
                'message': "Profile Not Found"
            }
        project = ProfileProject.query.filter_by(profile=profile_instance).filter_by(id=project_id).first()
        if project is None:
            return {
                'status': 0,
                'message': "Project Not Found"
            }
        db.session.delete(project)
        db.session.commit()
        db.session.refresh(profile_instance)
        profile = fill_profile(profile_instance)
        return {
            'status': 1,
            'profile': profile
        }


class AddProfileLang(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('lang', help='This field cannot be blank', required=True)
        self.parser.add_argument('level', help='This field cannot be blank', required=True)

    @login_required
    def post(self, profile_id):
        data = self.parser.parse_args()
        profile_instance = Profile.query.filter_by(user=current_user).filter_by(id=profile_id).first()
        if profile_instance is None:
            return {
                'status': 0,
                'message': "Profile Not Found"
            }
        lang = ProfileLang(lang=data['lang'], profile=profile_instance,
                           level=data['level'])
        db.session.add(lang)
        db.session.commit()
        db.session.refresh(lang)
        db.session.refresh(profile_instance)
        profile = fill_profile(profile_instance)
        return {
            'status': 1,
            'profile': profile
        }


class EditProfileLang(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('lang', help='This field cannot be blank', required=True)
        self.parser.add_argument('level', help='This field cannot be blank', required=True)

    @login_required
    def post(self, profile_id, lang_id):
        data = self.parser.parse_args()
        profile_instance = Profile.query.filter_by(user=current_user).filter_by(id=profile_id).first()
        if profile_instance is None:
            return {
                'status': 0,
                'message': "Profile Not Found"
            }
        lang = ProfileLang.query.filter_by(profile=profile_instance).filter_by(id=lang_id).first()
        if lang is None:
            return {
                'status': 0,
                'message': "Language Not Found"
            }
        lang.lang = data['lang']
        lang.level = data['level']
        db.session.add(lang)
        db.session.commit()
        db.session.refresh(lang)
        db.session.refresh(profile_instance)
        profile = fill_profile(profile_instance)
        return {
            'status': 1,
            'profile': profile
        }


class DeleteProfileLang(Resource):
    @login_required
    def post(self, profile_id, lang_id):
        profile_instance = Profile.query.filter_by(user=current_user).filter_by(id=profile_id).first()
        if profile_instance is None:
            return {
                'status': 0,
                'message': "Profile Not Found"
            }
        lang = ProfileLang.query.filter_by(profile=profile_instance).filter_by(id=lang_id).first()
        if lang is None:
            return {
                'status': 0,
                'message': "Language Not Found"
            }
        db.session.delete(lang)
        db.session.commit()
        db.session.refresh(profile_instance)
        profile = fill_profile(profile_instance)
        return {
            'status': 1,
            'profile': profile
        }


class EditProfile(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('first_name', help='This field cannot be blank', required=True)
        self.parser.add_argument('last_name', help='This field cannot be blank', required=True)
        self.parser.add_argument('title', help='This field cannot be blank', required=True)
        self.parser.add_argument('header', help='This field cannot be blank', required=True)
        self.parser.add_argument('commitment', help='This field cannot be blank', required=True)
        self.parser.add_argument('type_of_work', help='This field cannot be blank', required=True)
        self.parser.add_argument('image', help='This field cannot be blank', type=werkzeug.datastructures.FileStorage,
                                 location='files', required=False)
        self.parser.add_argument('cover', help='This field cannot be blank', type=werkzeug.datastructures.FileStorage,
                                 location='files', required=False)

    @login_required
    def post(self, profile_id):
        data = self.parser.parse_args()
        profile_instance = Profile.query.filter_by(user=current_user).filter_by(id=profile_id).first()
        if profile_instance is None:
            return {
                'status': 0,
                'message': "Profile Not Found"
            }
        image_filename = profile_instance.image
        if data['image']:
            image_filename = images.save(data['image'])
        cover_filename = profile_instance.cover
        if data['cover']:
            cover_filename = images.save(data['cover'])
        profile_instance.first_name = data['first_name']
        profile_instance.last_name = data['last_name']
        profile_instance.title = data['title']
        profile_instance.header = data['header']
        profile_instance.commitment = data['commitment']
        profile_instance.type_of_work = data['type_of_work']
        profile_instance.image = image_filename
        profile_instance.cover = cover_filename

        db.session.add(profile_instance)
        db.session.commit()
        db.session.refresh(profile_instance)
        profile = fill_profile(profile_instance)
        return {
            'status': 1,
            'profile': profile
        }


class DeleteProfile(Resource):
    @login_required
    def post(self, profile_id):
        profile_instance = Profile.query.filter_by(user=current_user).filter_by(id=profile_id).first()
        if profile_instance is None:
            return {
                'status': 0,
                'message': "Profile Not Found"
            }
        profile_instance.skills = []
        profile_instance.education = []
        profile_instance.jobs = []
        profile_instance.languages = []
        profile_instance.projects = []
        db.session.commit()
        db.session.refresh(profile_instance)
        db.session.delete(profile_instance)
        db.session.commit()
        return {
            'status': 1,
        }