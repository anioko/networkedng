from flask import (
    Blueprint,
    abort,
    flash,
    redirect,
    render_template,
    request,
    url_for,
    jsonify)
from flask_login import current_user, login_required
from flask_rq import get_queue

from app import db
from app.blueprints.management.forms import (
    InviteUserForm,
    NewUserForm,
    NewBusinessForm,
    InviteBusinessForm
)
from app.blueprints.admin.forms import ChangeProfileForm, ChangeUserNameForm, ChangeUserEmailForm, ChangeAccountTypeForm, ConfirmAccountForm
from app.utils import send_sms, register_to_marketplace
from app.decorators import admin_required, marketer_required
from app.email import send_email
from app.models import EditableHTML, Role, User, Post, Question, Organisation, Job, Message, ContactMessage, Service, Product, Event, Promo

management = Blueprint('management', __name__)


@management.route('/')
@login_required
@marketer_required
def index():
    """Admin dashboard page."""
    return render_template('management/index.html')


@management.route('/invitees')
@login_required
@marketer_required
def invited_users():
    """View all of a marketer's invited users."""
    #users = db.session.query(User).filter(current_user.full_name==User.invited_by_name).all()
    #orgs = db.session.query(Organisation).filter(Organisation.user_id==User.id).all()
    user_orgs = (db.session.query(Organisation)
                 .join(User, Organisation.user_id==User.id)
                 #.join(Organisation)
                 #.filter(Organisation.user_id==User.id)
                 .filter(current_user.full_name==User.invited_by_name)
                 ).all()
    return render_template(
        'management/invited_users.html', user_orgs=user_orgs)


@management.route('/users/unconfirmed')
@login_required
@marketer_required
def unconfirmed_users():
    """View all unconfirmed users."""
    users = db.session.query(User).filter(User.confirmed=='false', current_user.full_name==User.invited_by_name).all()
    return render_template(
        'management/unconfirmed_users.html', users=users)


@management.route('/new-user', methods=['GET', 'POST'])
@login_required
@marketer_required
def new_user():
    """Create a new user."""
    form = NewUserForm()
    password = form.password.data
    if form.validate_on_submit():
        user_instance = User(
            #role=form.role.data,
            first_name=form.first_name.data,
            last_name=form.last_name.data,
            email=form.email.data,
            password=form.password.data,
            area_code=form.area_code.data,
            mobile_phone=form.mobile_phone.data,
            invited_by_id = current_user.id,
            invitation_type = current_user.access_role,
            invited_by_name=current_user.full_name)
        db.session.add(user_instance)
        db.session.commit()

        user = User.query.filter(User.id==user_instance.id).first()
        otp = user.generate_confirmation_token()
        area_code = str(user.area_code)
        area_code = area_code.replace(' ', '')
        phone_number = str(user.mobile_phone)
        phone_number = phone_number.replace(' ', '')
        if str(area_code)[0] != '+':
            area_code = '+' + str(area_code)
        message = 'Hello {} your otp is {}'.format(user.full_name, str(otp))
        get_queue().enqueue(send_sms,to_phone=(area_code) + (phone_number), message=message)      
        flash('An otp has been sent to {}.'.format(str(area_code) + str(phone_number)) , 'warning')
        flash('User {} successfully created'.format(user.full_name),
              'form-success')
        return redirect(url_for('management.invited_users'))
    else:
        flash('Error! Data was not added.', 'error')
    return render_template('management/new_user.html', form=form)


@management.route('/invite-user', methods=['GET', 'POST'])
@login_required
@marketer_required
def invite_user():
    """Invites a new user to create an account and set their own password."""
    form = InviteUserForm()
    if form.validate_on_submit():
        user = User(
            role=form.role.data,
            first_name=form.first_name.data,
            last_name=form.last_name.data,
            email=form.email.data,
            invited_by = User.query.filter(User.id == current_user.id).first())
        db.session.add(user)
        db.session.commit()
        flash('User {} successfully invited'.format(user.full_name),
              'form-success')
    return render_template('admin/new_user.html', form=form)


@management.route('/invite-business', methods=['GET', 'POST'])
@login_required
@marketer_required
def invite_business():
    """Invites a new business user with business name and passwords."""
    form = InviteBusinessForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            #image_filename = images.save(request.files['logo'])
            #image_url = images.url(image_filename)
            user_instance = User(
                first_name=form.first_name.data,
                last_name=form.last_name.data,
                email=form.email.data,
                area_code=form.area_code.data,
                mobile_phone=form.mobile_phone.data,
                password=form.password.data,
                invited_by_id = current_user.id,
                invited_by_name = current_user.full_name,
                confirmed = True)
            print(user_instance)
            db.session.add(user_instance)
            db.session.commit()
                
            org = Organisation(
                user_id=user_instance.id,
                #image_filename=image_filename,
                #image_url=image_url,
                org_name=form.org_name.data,
                company_registration_number=form.company_registration_number.data,
                org_industry=form.org_industry.data,
                org_website=form.org_website.data,
                org_city=form.org_city.data,
                org_state=form.org_state.data,
                org_country="Nigeria",
                org_description=form.org_description.data,
                added_by=user_instance.full_name,
                marketer_id=current_user.id,
                marketer_name=current_user.full_name,
                added_type=current_user.access_role
                )
            
            print(org)
            db.session.add(org)
            db.session.commit()
            user = User.query.filter(User.id==user_instance.id).first()
            otp = user.generate_confirmation_token()
            area_code = str(user.area_code)
            area_code = area_code.replace(' ', '')
            phone_number = str(user.mobile_phone)
            phone_number = phone_number.replace(' ', '')
            if str(area_code)[0] != '+':
                area_code = '+' + str(area_code)
            message = 'Hello {} your otp is {}'.format(user.full_name, str(otp))
            get_queue().enqueue(send_sms,to_phone=(area_code) + (phone_number), message=message)      
            flash('An otp has been sent to {}.'.format(str(area_code) + str(phone_number)) , 'warning')
            flash('User {} successfully created'.format(user.full_name),
                  'form-success')
            return redirect(url_for('management.invited_users'))
        else:
            flash('Error! Data was not added.', 'error')
    return render_template('management/new_business.html', form=form)

@management.route('/users')
@login_required
@marketer_required
def registered_users():
    """View all registered users."""
    users = User.query.all()
    roles = Role.query.all()
    return render_template(
        'admin/registered_users.html', users=users, roles=roles)


@management.route('/user/<int:user_id>')
@management.route('/user/<int:user_id>/info')
@login_required
@marketer_required
def user_info(user_id):
    """View a user's profile."""
    user = User.query.filter_by(id=user_id).first()
    if user is None:
        abort(404)
    return render_template('management/manage_user.html', user=user)


@management.route('/user/<int:user_id>/change-email', methods=['GET', 'POST'])
@login_required
@marketer_required
def change_user_email(user_id):
    """Change a user's email."""
    user = User.query.filter_by(id=user_id).first()
    if user is None:
        abort(404)
    form = ChangeUserEmailForm()
    if form.validate_on_submit():
        user.email = form.email.data
        db.session.add(user)
        db.session.commit()
        flash('Email for user {} successfully changed to {}.'.format(
            user.full_name, user.email), 'form-success')
    return render_template('management/manage_user.html', user=user, form=form)



@management.route('/user/<int:user_id>/change-name', methods=['GET', 'POST'])
@login_required
@marketer_required
def change_user_name(user_id):
    """Change a user's first and last names."""
    user = User.query.filter_by(id=user_id).first()
    if user is None:
        abort(404)
    form = ChangeUserNameForm()
    if form.validate_on_submit():
        user.first_name = form.first_name.data
        user.last_name = form.last_name.data
        db.session.add(user)
        db.session.commit()
        flash('First and last names changes successfully', 'form-success')
    return render_template('management/manage_user.html', user=user, form=form)

@management.route('/user/<int:user_id>/edit-profile', methods=['GET', 'POST'])
@login_required
@admin_required
def change_profile_details(user_id):
    """Respond to existing user's request to change their profile details."""
    user = User.query.filter_by(id=user_id).first()
    if user is None:
        abort(404)
    form = ChangeProfileForm(obj=user)
    if request.method == 'POST':
        if form.validate_on_submit():
            if form.validate_on_submit():
                form.populate_obj(user)
                db.session.add(user)
                if request.files['photo']:
                    image_filename = images.save(request.files['photo'])
                    image_url = images.url(image_filename)
                    picture_photo = Photo.query.filter_by(user_id=current_user.id).first()
                    if not picture_photo:
                        picture_photo = Photo(
                            image_filename=image_filename,
                            image_url=image_url,
                            user_id=current_user.id,
                        )
                    else:
                        picture_photo.image_filename = image_filename
                        picture_photo.image_url = image_url
                    db.session.add(picture_photo)
                db.session.commit()
                flash('You have successfully updated the profile',
                      'success')
                return redirect(url_for('admin.registered_users'))
            else:
                flash('Unsuccessful.', 'warning')
    return render_template('account/edit_profile.html', form=form)

@management.route(
    '/user/<int:user_id>/change-account-type', methods=['GET', 'POST'])
@login_required
@marketer_required
def change_account_type(user_id):
    """Change a user's account type."""
    if current_user.id == user_id:
        flash('You cannot change the type of your own account. Please ask '
              'another administrator to do this.', 'error')
        return redirect(url_for('admin.user_info', user_id=user_id))

    user = User.query.get(user_id)
    if user is None:
        abort(404)
    form = ChangeAccountTypeForm()
    if form.validate_on_submit():
        user.role = form.role.data
        db.session.add(user)
        db.session.commit()
        flash('Role for user {} successfully changed to {}.'.format(
            user.full_name, user.role.name), 'form-success')
    return render_template('management/manage_user.html', user=user, form=form)


@management.route(
    '/user/<int:user_id>/change-account-confirmation', methods=['GET', 'POST'])
@login_required
@marketer_required
def change_account_confirmation(user_id):
    """Change a user's account type."""
    if current_user.id == user_id:
        flash('You cannot change the type of your own account. Please ask '
              'another administrator to do this.', 'error')
        return redirect(url_for('admin.user_info', user_id=user_id))

    user = User.query.get(user_id)
    if user is None:
        abort(404)
    form = ConfirmAccountForm()
    if form.validate_on_submit():
        user.confirmed = form.confirmed.data
        db.session.add(user)
        db.session.commit()
        flash('User confirmed', 'form-success')
    return render_template('management/manage_user.html', user=user, form=form)


@management.route('/user/<int:user_id>/delete')
@login_required
@marketer_required
def delete_user_request(user_id):
    user = User.query.filter_by(id=user_id).first()
    if user is None:
        abort(404)
    return render_template('management/manage_user.html', user=user)


@management.route('/user/<int:user_id>/_delete')
@login_required
@admin_required
def delete_user(user_id):
    """Delete a user's account."""
    if current_user.id == user_id:
        flash('You cannot delete your own account. Please ask another '
              'administrator to do this.', 'error')
    else:
        user = User.query.filter_by(id=user_id).first()
        db.session.delete(user)
        db.session.commit()
        flash('Successfully deleted user %s.' % user.full_name, 'success')
    return redirect(url_for('admin.registered_users'))


@management.route('/text/<text_type>', methods=['GET'])
@login_required
@marketer_required
def text(text_type):
    editable_html_obj = EditableHTML.get_editable_html(text_type)
    return jsonify({
        'status': 1,
        'editable_html_obj': editable_html_obj.serialize
    })


@management.route('/texts', methods=['POST', 'GET'])
@login_required
@marketer_required
def texts():
    editable_html_obj = EditableHTML.get_editable_html('contact')
    if request.method == 'POST':
        edit_data = request.form.get('edit_data')
        editor_name = request.form.get('editor_name')

        editor_contents = EditableHTML.query.filter_by(
            editor_name=editor_name).first()
        if editor_contents is None:
            editor_contents = EditableHTML(editor_name=editor_name)
        editor_contents.value = edit_data

        db.session.add(editor_contents)
        db.session.commit()
        flash('Successfully updated text.', 'success')
        return redirect(url_for('admin.texts'))
    return render_template('management/texts/index.html', editable_html_obj=editable_html_obj)


@management.route('/services', defaults={'page': 1}, methods=['GET'])
@management.route('/services/<int:page>', methods=['GET'])
@login_required
@marketer_required
def services(page):
    services_result = Service.query.paginate(page, per_page=100)
    return render_template('management/services/browse.html', services=services_result)

@management.route('/promos', defaults={'page': 1}, methods=['GET'])
@management.route('/promos/<int:page>', methods=['GET'])
@login_required
@marketer_required
def promos(page):
    promos_result = Promo.query.paginate(page, per_page=100)
    return render_template('management/promos/browse.html', promos=promos_result)

@management.route('/products', defaults={'page': 1}, methods=['GET'])
@management.route('/products/<int:page>', methods=['GET'])
@login_required
@marketer_required
def products(page):
    products_result = Product.query.paginate(page, per_page=100)
    return render_template('management/products/browse.html', products=products_result)

@management.route('/events', defaults={'page': 1}, methods=['GET'])
@management.route('/events/<int:page>', methods=['GET'])
@login_required
@marketer_required
def events(page):
    events_result = Event.query.paginate(page, per_page=100)
    return render_template('management/events/browse.html', events=events_result)

@management.route('/posts', defaults={'page': 1}, methods=['GET'])
@management.route('/posts/<int:page>', methods=['GET'])
@login_required
@marketer_required
def posts(page):
    posts_result = Post.query.paginate(page, per_page=100)
    return render_template('management/posts/browse.html', posts=posts_result)


@management.route('/post/<int:post_id>/_delete', methods=['POST'])
@login_required
@marketer_required
def delete_post(post_id):
    post = Post.query.filter_by(id=post_id).first()
    db.session.delete(post)
    db.session.commit()
    flash('Successfully deleted post.', 'success')
    return redirect(url_for('admin.posts'))


@management.route('/questions', defaults={'page': 1}, methods=['GET'])
@management.route('/questions/<int:page>', methods=['GET'])
@login_required
@marketer_required
def questions(page):
    questions_result = Question.query.paginate(page, per_page=100)
    return render_template('management/questions/browse.html', questions=questions_result)


@management.route('/question/<int:question_id>/_delete', methods=['POST'])
@login_required
@marketer_required
def delete_question(question_id):
    question = Question.query.filter_by(id=question_id).first()
    db.session.delete(question)
    db.session.commit()
    flash('Successfully deleted a question.', 'success')
    return redirect(url_for('admin.questions'))

@management.route('/service/<int:service_id>/_delete', methods=['POST'])
@login_required
@marketer_required
def delete_service(service_id):
    service = Service.query.filter_by(id=service_id).first()
    db.session.delete(service)
    db.session.commit()
    flash('Successfully deleted a service.', 'success')
    return redirect(url_for('admin.services'))

@management.route('/event/<int:event_id>/_delete', methods=['POST'])
@login_required
@marketer_required
def delete_event(event_id):
    event = Event.query.filter_by(id=event_id).first()
    db.session.delete(event)
    db.session.commit()
    flash('Successfully deleted an event.', 'success')
    return redirect(url_for('admin.events'))

@management.route('/product/<int:product_id>/_delete', methods=['POST'])
@login_required
@marketer_required
def delete_product(product_id):
    product = Product.query.filter_by(id=product_id).first()
    db.session.delete(product)
    db.session.commit()
    flash('Successfully deleted a product.', 'success')
    return redirect(url_for('admin.products'))

@management.route('/promo/<int:promo_id>/_delete', methods=['POST'])
@login_required
@marketer_required
def delete_promo(promo_id):
    promo = Promo.query.filter_by(id=promo_id).first()
    db.session.delete(promo)
    db.session.commit()
    flash('Successfully deleted a promo.', 'success')
    return redirect(url_for('admin.promos'))


@management.route('/orgs', defaults={'page': 1}, methods=['GET'])
@management.route('/orgs/<int:page>', methods=['GET'])
@login_required
@marketer_required
def orgs(page):
    orgs = Organisation.query.paginate(page, per_page=100)
    return render_template('management/orgs/browse.html', orgs=orgs)


@management.route('/org/<int:org_id>/_delete', methods=['POST'])
@login_required
@marketer_required
def delete_org(org_id):
    org = Organisation.query.filter_by(id=org_id).first()
    db.session.delete(org)
    db.session.commit()
    flash('Successfully deleted Organisation.', 'success')
    return redirect(url_for('admin.orgs'))


@management.route('/jobs', defaults={'page': 1}, methods=['GET'])
@management.route('/jobs/<int:page>', methods=['GET'])
@login_required
@marketer_required
def jobs(page):
    jobs_result = Job.query.paginate(page, per_page=100)
    return render_template('management/jobs/browse.html', jobs=jobs_result)


@management.route('/job/<int:job_id>/_delete', methods=['POST'])
@login_required
@marketer_required
def delete_job(job_id):
    job = Job.query.filter_by(id=job_id).first()
    db.session.delete(job)
    db.session.commit()
    flash('Successfully deleted Job.', 'success')
    return redirect(url_for('admin.jobs'))


@management.route('/messages', defaults={'page': 1}, methods=['GET'])
@management.route('/messages/<int:page>', methods=['GET'])
@login_required
@marketer_required
def messages(page):
    messages_result = Message.query.paginate(page, per_page=100)
    return render_template('management/messages/browse.html', messages=messages_result)


@management.route('/message/<int:message_id>/_delete', methods=['POST'])
@login_required
@marketer_required
def delete_message(message_id):
    message = Message.query.filter_by(id=message_id).first()
    db.session.delete(message)
    db.session.commit()
    flash('Successfully deleted Message.', 'success')
    return redirect(url_for('admin.messages'))


@management.route('/contact_messages', defaults={'page': 1}, methods=['GET'])
@management.route('/contact_messages/<page>', methods=['GET'])
@login_required
@marketer_required
def contact_messages(page):
    contact_messages_result = ContactMessage.query.paginate(page, per_page=100)
    return render_template('management/contact_messages/browse.html', contact_messages=contact_messages_result)


@management.route('/contact_message/<message_id>', methods=['GET'])
@login_required
@marketer_required
def view_contact_message(message_id):
    message = ContactMessage.query.filter_by(id=message_id).first_or_404()
    return render_template('management/contact_messages/view.html', contact_message=message)


@management.route('/contact_messages/<int:message_id>/_delete', methods=['POST'])
@login_required
@marketer_required
def delete_contact_message(message_id):
    message = ContactMessage.query.filter_by(id=message_id).first()
    db.session.delete(message)
    db.session.commit()
    flash('Successfully deleted Message.', 'success')
    return redirect(url_for('admin.contact_messages'))
