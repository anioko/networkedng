from flask_wtf import FlaskForm
from wtforms import ValidationError
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms.fields import BooleanField, PasswordField, StringField, SubmitField, IntegerField, SelectField, \
    TextAreaField
from wtforms.fields.html5 import EmailField
from wtforms import ValidationError, validators
from wtforms.validators import Email, EqualTo, InputRequired, Length, Optional, Required
from flask_wtf.file import FileField, FileAllowed
from flask_uploads import UploadSet, IMAGES
from app import db
from app.models import Role, User


from wtforms_alchemy import Unique, ModelForm
from wtforms_alchemy import model_form_factory
BaseModelForm = model_form_factory(FlaskForm)

images = UploadSet('images', IMAGES)

class sendSmsForm(FlaskForm):
    submit = SubmitField('Send')


class ChangeUserNameForm(FlaskForm):
    first_name = StringField(
        'First name', validators=[InputRequired()])
    last_name = StringField(
        'Last name', validators=[InputRequired()])
    submit = SubmitField('Update first name and last name')


class ChangeUserEmailForm(FlaskForm):
    email = EmailField(
        'New email', validators=[InputRequired(),
                                 Length(1, 64),
                                 Email()])
    submit = SubmitField('Update email')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Email already registered.')


class ChangeAccountTypeForm(FlaskForm):
    role = QuerySelectField(
        'New account type',
        validators=[InputRequired()],
        get_label='name',
        query_factory=lambda: db.session.query(Role).order_by('permissions'))
    submit = SubmitField('Update role')


class ConfirmAccountForm(FlaskForm):
    confirmed = BooleanField('True: Tick this checkbox to confirm the users account manually',
                             validators=[InputRequired()])
    submit = SubmitField('Confirm')

    
class InviteUserForm(FlaskForm):

    first_name = StringField(
        'First name', validators=[InputRequired(),
                                  Length(1, 64)])
    last_name = StringField(
        'Last name', validators=[InputRequired(),
                                 Length(1, 64)])
    email = EmailField(
        'Email', validators=[InputRequired(),
                             Length(1, 64),
                             Email()])
    area_code = StringField('Phone area code only e.g +234 or +44 or +1', validators=[InputRequired(), validators.Regexp('(?:[+]|^)(234|44|27|358|1)', message="Phone area code only e.g +234 or +44 or +1")])
    mobile_phone = IntegerField('Phone numbers only e.g 8123456789', validators=[InputRequired()])
    submit = SubmitField('Invite')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Email already registered.')


class NewUserForm(InviteUserForm):
    password = PasswordField(
        'Password',
        validators=[
            InputRequired(),
            EqualTo('password2', 'Passwords must match.')
        ])
    password2 = PasswordField('Confirm password', validators=[InputRequired()])

    submit = SubmitField('Create')
        

class InviteBusinessForm(FlaskForm):
    org_name = StringField('Organisation or business name', validators=[Required(), Length(1, 64)])
    first_name = StringField(
        'First name', validators=[InputRequired(),
                                  Length(1, 64)])
    last_name = StringField(
        'Last name', validators=[InputRequired(),
                                 Length(1, 64)])
    email = EmailField(
        'Email', validators=[InputRequired(),
                             Length(1, 64),
                             Email()])
    company_registration_number = StringField('e.g RC-1774748', [Length(max=11)])
    org_industry = StringField('Industry or Business Area or Sector', [Length(max=500)])
    org_city = StringField('City', validators=[InputRequired()])
    org_state = SelectField(u'Select State in Nigeria', choices=[
        ('Outside Nigeria','Outside Nigeria'),
        ('Abia State', 'Abia State'),
        ('Adamawa State', 'Adamawa State'),
        ('Akwa Ibom State', 'Akwa Ibom State'),
        ('Anambra State', 'Anambra State'),
        ('Bauchi State', 'Bauchi State'),
        ('Bayelsa State', 'Bayelsa State'),
        ('Benue State', 'Benue State'),
        ('Borno State', 'Borno State'),
        ('Cross River State', 'Cross River State'),
        ('Delta State', 'Delta State'),
        ('Ebonyi State', 'Ebonyi State'),
        ('Edo State', 'Edo State'),
        ('Ekiti State', 'Ekiti State'),
        ('Enugu State', 'Enugu State'),
        ('Gombe State', 'Gombe State'),
        ('Imo State', 'Imo State'),
        ('Jigawa State', 'Jigawa State'),
        ('Kaduna State', 'Kaduna State'),
        ('Kano State', 'Kano State'),
        ('Katsina State', 'Katsina State'),
        ('Kebbi State', 'Kebbi State'),
        ('Kogi State', 'Kogi State'),
        ('Kwara State', 'Kwara State'),
        ('Lagos State', 'Lagos State'),
        ('Nasarawa State', 'Nasarawa State'),
        ('Niger State', 'Niger State'),
        ('Ogun State', 'Ogun State'),
        ('Ondo State', 'Ondo State'),
        ('Osun State', 'Osun State'),
        ('Oyo State', 'Oyo State'),
        ('Plateau State', 'Plateau State'),
        ('Rivers State', 'Rivers State'),
        ('Sokoto State', 'Sokoto State'),
        ('Taraba State', 'Taraba State'),
        ('Yobe State', 'Yobe State'),
        ('Zamfara State', 'Zamfara State')])
    area_code = StringField('Phone area code only e.g +234 or +44 or +1', validators=[InputRequired(), validators.Regexp('(?:[+]|^)(234|44|27|358|1)', message="Phone area code only e.g +234 or +44 or +1")])
    mobile_phone = IntegerField('Phone numbers only e.g 8123456789', validators=[InputRequired()])
    org_website = StringField('www.example.com')
    org_description = TextAreaField('Description')
    password = PasswordField(
        'Password',
        validators=[
            InputRequired(),
            EqualTo('password2', 'Passwords must match.')
        ])
    password2 = PasswordField('Confirm password', validators=[InputRequired()])
    submit = SubmitField('Invite')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Email already registered.')

class NewBusinessForm(InviteBusinessForm):
    password = PasswordField(
        'Password',
        validators=[
            InputRequired(),
            EqualTo('password2', 'Passwords must match.')
        ])
    password2 = PasswordField('Confirm password', validators=[InputRequired()])

    submit = SubmitField('Create')





